-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Oct 30, 2017 at 02:07 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `openlearning`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE IF NOT EXISTS `announcements` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `content` text NOT NULL,
  `date_added` datetime NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `title`, `content`, `date_added`, `user_id`) VALUES
(1, 'hi', 'sdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdfsdfaksdfjlkasjdfsdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdfsdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdfsdfaksdfjlkasjdfsdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdfv\r\nsdfaksdfjlkasjdfsdfaksdfjlkasjdfsdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdfsdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdfsdfaksdfjlkasjdfsdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdfvsdfaksdfjlkasjdfsdfaksdfjlkasjdfsdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdfsdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdfsdfaksdfjlkasjdfsdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdfvsdfaksdfjlkasjdfsdfaksdfjlkasjdfsdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdfsdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdfsdfaksdfjlkasjdfsdfaksdfjlkasjdf\r\nsdfaksdfjlkasjdfv', '0000-00-00 00:00:00', 1),
(3, 'Exam tomorrow!', 'test', '0000-00-00 00:00:00', 1),
(4, 'hello', '123\r\n', '2017-08-27 11:08:00', 1),
(5, '123', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>hii</strong></p>\r\n</body>\r\n</html>', '2017-08-27 21:52:00', 1),
(6, 'Test', '<!DOCTYPE html>\r\n<html>\r\n<head>\r\n</head>\r\n<body>\r\n<p><strong>dfsdfsdf</strong></p>\r\n<p>&nbsp;</p>\r\n</body>\r\n</html>', '2017-08-28 08:23:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE IF NOT EXISTS `modules` (
  `id` int(11) NOT NULL,
  `filename` varchar(200) NOT NULL,
  `path` varchar(500) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `filename`, `path`, `user_id`, `subject_id`, `date_added`) VALUES
(17, '5.html', 'C:\\xampp\\htdocs\\ua\\app\\webroot\\modules\\subjects\\1\\5.html', 1, 1, '2017-08-27 23:08:00'),
(18, '20623576_10207349419932904_1008692010_o.jpg', 'C:\\xampp\\htdocs\\ua\\app\\webroot\\modules\\subjects\\1\\20623576_10207349419932904_1008692010_o.jpg', 1, 1, '2017-08-27 23:09:00'),
(20, 'putty.exe', 'C:\\xampp\\htdocs\\ua\\app\\webroot\\modules\\modules\\subjects\\3\\putty.exe', 1, 3, '2017-08-27 23:41:00'),
(21, '5.html', 'C:\\xampp\\htdocs\\ua\\app\\webroot\\modules\\modules\\subjects\\1\\5.html', 1, 1, '2017-08-28 00:49:00'),
(22, '20623576_10207349419932904_1008692010_o (1).jpg', 'C:\\xampp\\htdocs\\ua\\app\\webroot\\modules\\modules\\subjects\\2\\20623576_10207349419932904_1008692010_o (1).jpg', 1, 2, '2017-08-28 00:50:00'),
(23, 'student_subjects.csv', 'C:\\xampp\\htdocs\\ua\\app\\webroot\\\\modules\\subjects\\2\\student_subjects.csv', 1, 2, '2017-08-28 00:51:00'),
(24, '{$teacher[-User-][-last_name-]}_$teacher[-User-][-first_name-]}_subjects.csv', 'C:\\xampp\\htdocs\\ua\\app\\webroot\\\\modules\\subjects\\1\\{$teacher[-User-][-last_name-]}_$teacher[-User-][-first_name-]}_subjects.csv', 1, 1, '2017-08-28 11:54:00');

-- --------------------------------------------------------

--
-- Table structure for table `quizzes`
--

CREATE TABLE IF NOT EXISTS `quizzes` (
  `id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `date_added` date NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `no_of_items` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quizzes`
--

INSERT INTO `quizzes` (`id`, `subject_id`, `date_added`, `user_id`, `title`, `no_of_items`) VALUES
(7, 1, '0000-00-00', 1, 'Math', 3),
(8, 0, '0000-00-00', 0, 'Math', 2),
(9, 0, '0000-00-00', 0, 'Math', 3),
(10, 1, '0000-00-00', 1, 'Math 2', 1),
(11, 1, '2017-10-30', 1, 'This is a test', 10),
(12, 1, '2017-10-30', 1, 'HIIII', 2);

-- --------------------------------------------------------

--
-- Table structure for table `quizzes_items`
--

CREATE TABLE IF NOT EXISTS `quizzes_items` (
  `id` int(11) NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `question` text NOT NULL,
  `a` text NOT NULL,
  `b` text NOT NULL,
  `c` text NOT NULL,
  `d` text NOT NULL,
  `answer` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `quizzes_items`
--

INSERT INTO `quizzes_items` (`id`, `quiz_id`, `question`, `a`, `b`, `c`, `d`, `answer`) VALUES
(14, 10, 'e', '4', '1', '3', '4', 'a'),
(16, 7, '1+1', '1', '2', '3', '4', 'b'),
(17, 7, '2+2', '1', '3', '4', '2', 'c'),
(18, 7, '5*9', '10', '45', '20', '12', 'b'),
(19, 12, 'kl;k', ';lk', ';lk', 'l;k', ';lk', 'a'),
(20, 12, ''';l', 'l;', 'l;', 'l;', ';l;', 'b');

-- --------------------------------------------------------

--
-- Table structure for table `students_answers`
--

CREATE TABLE IF NOT EXISTS `students_answers` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `answers` text NOT NULL,
  `quiz_id` int(11) NOT NULL,
  `score` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students_answers`
--

INSERT INTO `students_answers` (`id`, `user_id`, `answers`, `quiz_id`, `score`) VALUES
(1, 5, '{"16":"b","17":"c","18":"a"}', 7, 0),
(4, 5, '{"14":"d"}', 10, 0),
(5, 5, '{"19":"a","20":"b"}', 12, 0);

-- --------------------------------------------------------

--
-- Table structure for table `students_subjects`
--

CREATE TABLE IF NOT EXISTS `students_subjects` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `grade` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `students_subjects`
--

INSERT INTO `students_subjects` (`id`, `user_id`, `subject_id`, `grade`) VALUES
(56, 5, 0, ''),
(57, 5, 3, ''),
(58, 8, 3, ''),
(59, 9, 3, ''),
(62, 8, 1, '1'),
(67, 5, 1, '2.0');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE IF NOT EXISTS `subjects` (
  `id` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` text NOT NULL,
  `school_year` varchar(100) NOT NULL,
  `semester` varchar(100) NOT NULL,
  `user_id` int(11) NOT NULL,
  `schedule` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `title`, `description`, `school_year`, `semester`, `user_id`, `schedule`) VALUES
(1, 'Math', 'hiiii', '2017-2018', '1st', 6, '8:20Am'),
(2, 'Calculus', 'this', '2017-2018', '1st', 7, ''),
(3, 'Trigonometry', 'TEst', '2017-2018', 'first sem', 10, ''),
(4, 'test', 'test', '2017-2018', 'test', 6, '8AM - 9AM'),
(5, 'sd', 'sd', '234', '132123', 6, '23'),
(6, 'lkjlkj', 'lkjlkj', '123', '1st', 10, '10-2:00'),
(7, 'Math', 'Math', 'k;lk;', 'l;k;l', 7, 'k;lk;l'),
(8, 'test2', 'test2', '123', '2nd', 6, '10AM - 12AM');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `password` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `birthdate` date NOT NULL,
  `is_enrolled` varchar(20) NOT NULL,
  `gender` varchar(8) NOT NULL,
  `temp_password` varchar(100) NOT NULL,
  `registration_number` varchar(100) NOT NULL,
  `middle_initial` varchar(20) NOT NULL,
  `receipt_no` varchar(100) NOT NULL,
  `date_enrolled` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_name`, `password`, `type`, `first_name`, `last_name`, `birthdate`, `is_enrolled`, `gender`, `temp_password`, `registration_number`, `middle_initial`, `receipt_no`, `date_enrolled`) VALUES
(1, 'admin', '8e9a540e9377dbb5f263d3ee0409d4e46b3fb562', 'admin', 'Admin', 'Admin', '0000-00-00', '0', '', '', '', '', '', '0000-00-00'),
(5, 'John.Doe_411sq', '741b5c2cc6d05183fe9a50446c8c2f23b93d483f', 'student', 'John', 'Doe', '2001-09-11', 'Pending', 'F', 'n8t1rv', '', '', '', '0000-00-00'),
(6, 'teacher.1_ewpb5', 'c2d0c0699096fe1864f3e8e2d48c7db25aef2d1c', 'teacher', 'teacher', '1', '1995-12-15', '', 'M', 'amgx5w', '', '', '', '0000-00-00'),
(7, 'Teacher .two_1asbh', '93ef95eafd199151ce0f4906d5bbb845e96a1869', 'teacher', 'Teacher ', 'two', '2002-10-13', '', 'F', 'onjqes', '', '', '', '0000-00-00'),
(8, 'Stephen.Test_55wjs', '2e9dcd7d268580d6681aa8fd1823a3d92643dc4c', 'student', 'Stephen', 'Test', '2000-08-11', 'Enrolled', 'M', 'ttwyks', '', '', '', '0000-00-00'),
(9, 'Jesseth.Test_6tjn1', 'f78fdadf2822bebd033bd2a75e281e08f3fda678', 'student', 'Jesseth', 'Test', '1998-09-12', 'Enrolled', 'F', 'nb1u84', '', '', '', '0000-00-00'),
(10, 'teacher 3.teacher_torup', 'b1af1a6123e6798802c1194e7cd426978f0039d6', 'teacher', 'teacher 3', 'teacher', '1998-10-13', '', 'M', 'a11un6', '', '', '', '0000-00-00'),
(11, 'asdasd`.dadf_6qseu', 'aca2fa5d746f64d076a256d15e85304a37c6dbff', 'student', 'asdasd`', 'dadf', '1997-11-13', 'Enrolled', 'M', 'weoyl9', '', '', '', '0000-00-00'),
(12, 'Ena Fleurence.Cahilig_j7wd4', 'bed4a0598f7cac43ed648f3c17b4bdc3e1d85208', 'student', 'Ena Fleurence', 'Cahilig', '1978-11-16', 'Enrolled', 'F', 'eem2z9', '123123', 'C', '123', '0000-00-00'),
(13, 'qwe.qwe_ri5el', '2d1da5198f6e041ef4eb6faaf65c817e8586508f', 'student', 'qwe', 'qwe', '1998-08-14', 'Enrolled', 'M', 'amlqjw', 'qwe', 'qwe', 'qwe', '0000-00-00'),
(14, 'adsf.adf_b4ry9', 'd198e8754219efc359096810fb2eff2cc5e9f57b', 'student', 'adsf', 'adf', '1998-12-12', 'Enrolled', 'M', 'wmqc92', '1', 'asdf', 'aadf', '0000-00-00'),
(15, 'adsf.adf_e5fbc', '750eb3c189c50a066663a523e5bf442eb7753caa', 'student', 'adsf', 'adf', '1996-10-16', 'Enrolled', 'M', '4e80xi', 'asdf', 'adf', 'aaf', '0000-00-00'),
(16, 'qwe.qwe_xhmjy', '4da5c85ca9e8400e093781fa3942c62567045484', 'student', 'qwe', 'qwe', '1997-08-14', 'Enrolled', 'M', 'idsr0h', '123', 'qwe', '123', '2017-10-28');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quizzes`
--
ALTER TABLE `quizzes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quizzes_items`
--
ALTER TABLE `quizzes_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students_answers`
--
ALTER TABLE `students_answers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students_subjects`
--
ALTER TABLE `students_subjects`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `user_id` (`user_id`,`subject_id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `quizzes`
--
ALTER TABLE `quizzes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `quizzes_items`
--
ALTER TABLE `quizzes_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `students_answers`
--
ALTER TABLE `students_answers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `students_subjects`
--
ALTER TABLE `students_subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
