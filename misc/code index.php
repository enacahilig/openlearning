Codes Index

Convetion  of CakePHP

Note: connect to DB
	app/Config/database.php

1. Home Page
	- App/View/Layouts/home.ctp

2. Login
	Model: User
	Controller: app/Controller/UsersController.php - login()
	View: app/View/Users/login.ctp 

3. Logout
	Model: User
	Controller: app/Controller/UsersController.php - logout()

4. Students List
	Model:  User
	Controller: app/Controller/UsersController.php - students_list()
	View: app/View/Users/students_list.ctp 

5. Add Student 
	Model:  User
	Controller: app/Controller/UsersController.php - add_student()
	View: app/View/Users/add_student.ctp 

6. Edit Student 
	Model:  User
	Controller: app/Controller/UsersController.php - edit_student()
	View: app/View/Users/edit_student.ctp 

7. View Students Subjects
	Model:  Subject
	Controller: app/Controller/SubjectsController.php - students_subjects()
	View: app/View/Subjects/students_subjects.ctp	

8. Export Students
	Model:  User
	Controller: app/Controller/UsersController.php - export_students()

9. Export Students Subjects
	Model:  Subject
	Controller: app/Controller/SubjectsController.php - export_students_subjects()

10. Add Teacher 
	Model:  User
	Controller: app/Controller/UsersController.php - add_teacher()
	View: app/View/Users/add_teacher.ctp 

11. Edit Teacher 
	Model:  User
	Controller: app/Controller/UsersController.php - edit_teacher()
	View: app/View/Users/edit_teacher.ctp 

12. Teacher Subjects (View Teacher Subjects)
	Model:  Subject
	Controller: app/Controller/SubjectsController.php - teacher_subjects()
	View: app/View/Subjects/teacher_subjects.ctp	

13. Export Teachers
	Model:  User
	Controller: app/Controller/UsersController.php - export_teacher()

14. Export Teacher Subjects
	Model:  Subject
	Controller: app/Controller/SubjectsController.php - export_teacher_subjects()

- index()
	View: app/View/Announcements/index.ctp15. Add Announcement
	Model:  Announcement
	Controller: app/Controller/AnnouncementsController.php - add()
	View: app/View/Announcements/add.ctp

16. Edit Announcement
	Model:  Announcement
	Controller: app/Controller/AnnouncementsController.php - edit()
	View: app/View/Announcements/edit.ctp

17. Delete Announcement
	Model:  Announcement
	Controller: app/Controller/AnnouncementsController.php - delete()

18. List of Announcement
	Model:  Announcement
	Controller: app/Controller/AnnouncementsController.php 

19. Subject View
	Model:  Subject
	Controller: app/Controller/SubjectsController.php - view()
	View: app/View/Subjects/view.ctp

20. Add Subject
	Model:  Subject
	Controller: app/Controller/SubjectsController.php - add()
	View: app/View/Subjects/add.ctp

21. Edit Subject
	Model:  Subject
	Controller: app/Controller/SubjectsController.php - edit()
	View: app/View/Subjects/edit.ctp

22. Add Grade
	Model:  Subject
	Controller: app/Controller/SubjectsController.php - add_grade()
	View: app/View/Subjects/add_grade.ctp

23. Enroll Student
	Model:  Subject
	Controller: app/Controller/SubjectsController.php - enroll_students()
	View: app/View/Subjects/enroll_students.ctp

24. Add Quiz
	Model:  Quiz
	Controller: app/Controller/QuizzesController.php - add()
	View: app/View/Quizzes/add.ctp

25. Edit Quiz
	Model:  Quiz
	Controller: app/Controller/QuizzesController.php - edit()
	View: app/View/Quizzes/edit.ctp

26. Delete Quiz
	Model:  Quiz
	Controller: app/Controller/QuizzesController.php - delete()

27. List of Quizzes
	Model:  Subject
	Controller: app/Controller/SubjectsController.php - quizzes()
	View: app/View/Subjects/quizzes.ctp

28. List of Subject's Modules
	Model:  Subject
	Controller: app/Controller/SubjectsController.php - modules()
	View: app/View/Subjects/modules.ctp

29. Upload Module
	Model:  Subject
	Controller: app/Controller/SubjectsController.php - upload_modules()

30. Dowload Module
	Model:  Subject
	Controller: app/Controller/SubjectsController.php - download()

31. Delete Module
	Model:  Subject
	Controller: app/Controller/SubjectsController.php - delete_module()

32. Change password
	Model:  User
	Controller: app/Controller/UsersController.php - account()
	View: app/View/Users/account.ctp


UPDATES:

1.  List of Quizzes
	Model:  Subject
	Controller: app/Controller/SubjectsController.php - quizzes()
	View: app/View/Subjects/quizzes.ctp
		- added the score
		- change the layout of view quiz

2.  Students List
	Model:  User
	Controller: app/Controller/UsersController.php - students_list()
	View: app/View/Users/students_list.ctp 

	-added search

2.  Teachers List
	Model:  User
	Controller: app/Controller/UsersController.php - students_list()
	View: app/View/Users/teachers_list.ctp 

	-added search

3.  Subjects
	Model:  Subject
	Controller: app/Controller/SubjectsController.php - index()
	View: app/View/Subjects/index.ctp

	-added search

4. Add Quiz
	Model:  Quiz
	Controller: app/Controller/QuizzesController.php - add()
	View: app/View/Quizzes/add.ctp

	-change view, added add_items page (in charge of adding questions):

	Model:  Quiz
	Controller: app/Controller/QuizzesController.php - add_items()
	View: app/View/Quizzes/add_items.ctp 

4. Edit Quiz
	Model:  Quiz
	Controller: app/Controller/QuizzesController.php - edit()
	View: app/View/Quizzes/edit.ctp

	-change view, added edit_items page (in charge of editing questions):

	Model:  Quiz
	Controller: app/Controller/QuizzesController.php - edit_items()
	View: app/View/Quizzes/edit_items.ctp 

5. Take Quiz
	Model:  Quiz
	Controller: app/Controller/QuizzesController.php - take()
	View: app/View/Quizzes/take.ctp

	- display questions and blank fields for answer

6. Added Back button (top right of the screen)
	- code can be found in App/View/Layout/default.ctp - look for "BACK"

SUMMARY of UPDATES (please see old code index above, since I used the same files and added new codes on the same functions):

Quiz - done

remove delete - done
admin cannot add grades - done
admin can view quizzes - donr

adding students, add middle initial, receipt kag date enrolled - done

if same student added, check if it exists - done

search module to students? - done

all users should have back button - done

login by teacher, student, admin - done


retype pass if we have to change - done

If subject is added, title and description should be the same
and check if exists - done

Announcements, notify users - recommendation
