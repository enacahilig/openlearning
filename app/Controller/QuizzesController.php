<?php
/*all the functions for users adre here*/

App::uses('AppController', 'Controller');
class QuizzesController extends AppController {

	public function add($subject_id){
		$this->set("subject_id",$subject_id);
		if ($this->request->is('post')) {
			$this->Quiz->create();
			$this->request->data["Quiz"]["subject_id"] = $subject_id;
			$this->request->data["Quiz"]["user_id"] = $this->Auth->user("id");
			$this->request->data["Quiz"]["date_added"] = date("Y-m-d H:i");
			$this->Quiz->save($this->request->data);

			$this->Session->setFlash(__('You have successfully added this quiz.'), 'default', array('class' => 'alert alert-success'));
			$id=$this->Quiz->getLastInsertId();
			$this->redirect("/quizzes/add_items/{$id}");
		}
	}

	public function add_items($quiz_id){
		$this->loadModel("QuizzesItem");
		if ($this->request->is('post')) {
			
			foreach($this->request->data["Quiz"] as $question){
				$data = array(
					"quiz_id"=>$quiz_id,
					"question"=>$question["question"],
					"a"=>$question["a"],
					"b"=>$question["b"],
					"c"=>$question["c"],
					"d"=>$question["d"],
					"answer"=>strtolower($question["answer"])
				);
				$this->QuizzesItem->create();
				$this->QuizzesItem->save($data);
			}
			$this->redirect("view/".$quiz_id);
		}
		$quiz = $this->Quiz->findById($quiz_id);
		$this->set("quiz", $quiz);
	}

	public function edit_items($quiz_id){
		$this->loadModel("QuizzesItem");	
		if ($this->request->is('post')  || $this->request->is("put")) {
			$this->QuizzesItem->deleteAll(array("quiz_id"=>$quiz_id));
			foreach($this->request->data["Quiz"] as $question){
				$data = array(
					"quiz_id"=>$quiz_id,
					"question"=>$question["question"],
					"a"=>$question["a"],
					"b"=>$question["b"],
					"c"=>$question["c"],
					"d"=>$question["d"],
					"answer"=>strtolower($question["answer"])
				);
				$this->QuizzesItem->id = $question["id"];
				$this->QuizzesItem->save($data);
			}
			$this->redirect("view/".$quiz_id);
		}
		else{
			$items= $this->QuizzesItem->findAllByQuizId($quiz_id);

			foreach ($items as $key => $item) {
				$this->request->data["Quiz"][$key] = $item["QuizzesItem"];
			}
		}

		$quiz = $this->Quiz->findById($quiz_id);
		$this->set("quiz", $quiz);
	}

	public function view($quiz_id){
		$this->Quiz->bindModel(array("hasMany"=>array("QuizzesItem")));
		$quiz = $this->Quiz->findById($quiz_id);
		$this->set("quiz", $quiz);
	}

	public function result($quiz_id, $student_id){
		$this->Quiz->bindModel(array("hasMany"=>array("QuizzesItem")));
		$quiz = $this->Quiz->findById($quiz_id);


		$this->loadModel("StudentsAnswer");
		$this->loadModel("QuizzesItem");
		
		$result=$this->StudentsAnswer->findByQuizIdAndUserId($quiz_id, $student_id);

		$answers = json_decode($result["StudentsAnswer"]["answers"], true);

		foreach ($quiz["QuizzesItem"] as $key => $item) {
		 	$quiz["QuizzesItem"][$key]["student_answer"]=$answers[$item["id"]];
		}


		$score = 0;

		if($answers){
			foreach ($answers as $item_id => $answer) {
				# code...
				$correct = $this->QuizzesItem->findByIdAndAnswer($item_id, strtolower($answer));

				if($correct){
					$score++;
				}
			}

			$quiz["Score"] = "{$score}/{$quiz["Quiz"]["no_of_items"]}";	
		}
		else{
			$quiz["Score"] = "";
		}

		//$this->set("quiz", $quiz);
		$this->loadModel("User");
		$student = $this->User->findById($student_id);
		$this->loadModel("Subject");
		$subject = $this->Subject->findById($quiz["Quiz"]["subject_id"]);
		$this->set("subject", $subject);
		$this->set("student", $student);
		$this->set("quiz", $quiz);
	}

	public function edit($id){

		$quiz = $this->Quiz->findById($id);
		if($this->request->is("post") || $this->request->is("put")){
			$this->Quiz->id=$id;
			$this->Quiz->save($this->request->data);

			$this->Session->setFlash(__('You have successfully updated this quiz.'), 'default', array('class' => 'alert alert-success'));

			$this->redirect("/quizzes/edit_items/{$id}");
		}
		else{
			
			$this->request->data = $quiz;
			$this->set("quiz", $quiz);
			$this->set("subject_id",$quiz["Quiz"]["subject_id"]);
		}
	}

	public function delete($id){
		$quiz = $this->Quiz->findById($id);
		if($this->Quiz->exists($id)){
			$this->Quiz->id = $id;
			$this->Quiz->delete();
			$this->Session->setFlash(__('You have successfully delete this Quiz.'), 'default', array('class' => 'alert alert-success'));
			$this->redirect("/subjects/quizzes/{$quiz["Quiz"]["subject_id"]}");
		}
	}

	public function take($quiz_id){
		$this->Quiz->bindModel(array("hasMany"=>array("QuizzesItem")));
		$quiz = $this->Quiz->findById($quiz_id);
		$this->set("quiz", $quiz);

		

		$this->loadModel("StudentsAnswer");

		$exist = $this->StudentsAnswer->findByQuizIdAndUserId($quiz_id, $this->Auth->user("id"));


		if($exist){
			$start_time = $exist["StudentsAnswer"]["start_time"];

			if($exist["StudentsAnswer"]["done"]){
				$this->Session->setFlash(__('You have already taken this Quiz.'), 'default', array('class' => 'alert alert-danger'));
				$this->redirect("/subjects/quizzes/{$quiz["Quiz"]["subject_id"]}");
			}

			$temp = [];
			if($exist){
				$current_answers = json_decode($exist["StudentsAnswer"]["answers"], true);
				foreach($quiz["QuizzesItem"] as $index => $question){
					$this->request->data["Quiz"][$index]["answer"]=$current_answers[$question["id"]];
				}
			}

		}
		else{
			$start_time = date("Y-m-d H:i");
			$this->StudentsAnswer->create();
			$this->StudentsAnswer->save(
				array(
					"quiz_id"=>$quiz_id,
					"user_id"=>$this->Auth->user("id"),
					"start_time" => $start_time 
				)
			);
		}
		$date = new DateTime($start_time);
		$date->modify("+".$quiz["Quiz"]["duration"]." minutes");
		$this->set("end_date", $date->format('Y-m-d h:i A'));		

		
		if ($this->request->is('post')) {

			$temp_answer = [];

			foreach($this->request->data["Quiz"] as $answer){
				$temp_answer[$answer["question_id"]] = $answer["answer"];
			}

			

			$answers = json_encode($temp_answer);

			if($exist){
				$this->StudentsAnswer->save(
					array(
						"id"=>$exist["StudentsAnswer"]["id"],
						"quiz_id"=>$quiz_id,
						"user_id"=>$this->Auth->user("id"),
						"answers"=>$answers,
						"done"=>1
					)
				);
			}
			else{
				$this->StudentsAnswer->create();
				$this->StudentsAnswer->save(
					array(
						"quiz_id"=>$quiz_id,
						"user_id"=>$this->Auth->user("id"),
						"answers"=>$answers,
						"done"=>1
					)
				);
			}

		
			$this->redirect("/subjects/quizzes/".$quiz["Quiz"]["subject_id"]);
		}		
		
	}

	public function ajax_save_user_answer(){
		
		$question_id = $this->params["named"]["question_id"];
		$answer = $this->params["named"]["answer"];
		$quiz_id = $this->params["named"]["quiz_id"];

		$this->loadModel("StudentsAnswer");

		$student_answer = $this->StudentsAnswer->findByUserIdAndQuizId($this->Auth->user("id"),$quiz_id);
		$result = array(
			"error" => 0,
			"message"=> "",
			"student_answer"=> $student_answer
		);


		if($student_answer){

			$answers = $student_answer["StudentsAnswer"]["answers"];
			$answers = json_decode($answers, true);
			$answers[$question_id] = $answer;

			$answers = json_encode($answers);
			$student_answer["StudentsAnswer"]["answers"] = $answers;

			$this->StudentsAnswer->save($student_answer["StudentsAnswer"]);

			$result = array(
			"error" => 0,
			"message"=> "success",
			"student_answer"=> $student_answer
		);

		}
		
		$this->layout = "ajax";
		die(json_encode($result));

	}



};

?>