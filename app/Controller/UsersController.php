<?php
/*all the functions for users adre here*/

App::uses('AppController', 'Controller');
class UsersController extends AppController {
	public function login(){	
		if ($this->request->is('post')) {

			$user_exist = $this->User->findByUserNameAndType($this->request->data['User']['username'], $this->request->data['User']['type']);
			

			if ($user_exist){
				if($user_exist['User']['password']==$this->Auth->password($this->request->data['User']['password'])){
					
				    $this->Auth->login($user_exist['User']);
				    $this->Session->setFlash(__('You have successfully logged in.'), 'default', array('class' => 'alert alert-success'));
				   
				   if($this->Auth->user("type") == "admin"){
				   		$this->redirect("students_list");	
				   }
				   else if($this->Auth->user("type") == "teacher"){
				   		$this->redirect("/subjects/");	
				   }
				   else{
				   		$this->redirect("/announcements/");	
				   }
				   
				}
				else{
					
					$this->Session->setFlash(__('Invalid password. Please try again.'), 'default', array('class' => 'alert alert-danger'));
  	  			}
			}
	        else{
				$this->Session->setFlash(__('Invalid username. Please try again.'), 'default', array('class' => 'alert alert-danger'));
  	  		}
  	  	}
		$this->layout = "login";
	}
	
	public function logout(){
   		$this->redirect($this->Auth->logout());
 	}

	public function home(){
		$this->layout = "home";	
	}

	public function dashboard(){
		$auth = $this->Auth->user();
		if($auth){
			if($auth["type"] == "admin" || $auth["type"] == "teacher"){
				$this->redirect("students_list");
			}
		}
		else{
			$this->redirect("login");
		}
	}


	public function students_list(){
		if ($this->request->is('post')) {

			//added this for search
			$keyword = isset($this->data['User']['keyword']) ? $this->data['User']['keyword']:'';
			$conditions = "(User.first_name LIKE '%$keyword%' OR User.last_name  LIKE '%$keyword%' OR CONCAT(User.first_name,' ', User.last_name) LIKE '%$keyword%' OR User.receipt_no LIKE '%$keyword%' OR User.registration_number LIKE '%$keyword%' OR User.middle_initial LIKE '%$keyword%') AND User.type='student'";
			$order = "User.id DESC";
			$students = $this->User->find('all', compact('conditions', 'order'));
		}
		else{
			$conditions = "User.type = 'student'";
			$order = "User.id DESC";
			$students = $this->User->find('all', compact('conditions', 'order'));
		}
		
		$this->set("students", $students);
	}

	public function ajax_check_registration_number(){
		
		$registration_number = $this->params["named"]["keyword"];
		$registration_number_exist = $this->User->findByRegistrationNumber($registration_number);
		$result = array(
			"error" => 0,
			"message"=> "",
			"registration_number"=>$registration_number
		);
		if($registration_number_exist){
			$result = array(
				"error" => 1,
				"message"=> "This registration_number already exists.",
				"registration_number"=>$registration_number
			);
		}
		
		$this->layout = "ajax";
		die(json_encode($result));

	}

	public function add_student(){
		if ($this->request->is('post')) {

			//check if student exists

			$this->request->data["User"]["first_name"] = ucwords($this->request->data["User"]["first_name"] );
			$this->request->data["User"]["middle_initial"] = ucwords($this->request->data["User"]["middle_initial"] );
			$this->request->data["User"]["last_name"] = ucwords($this->request->data["User"]["last_name"] );
			$student_exist = $this->User->findByFirstNameAndMiddleInitialAndLastName($this->request->data["User"]["first_name"], $this->request->data["User"]["middle_initial"], $this->request->data["User"]["last_name"] );

			if($student_exist){
				$this->Session->setFlash(__('This student already exists.'), 'default', array('class' => 'alert alert-danger'));
			}

			$registration_number_exist = $this->User->findByRegistrationNumber($this->request->data["User"]["registration_number"]);

			if($registration_number_exist){
				$this->Session->setFlash(__('This registration number already exists.'), 'default', array('class' => 'alert alert-danger'));
			}

			else{
				$this->request->data["User"]["type"]="student";
				$this->request->data["User"]["birthdate"] = $this->request->data['User']['year'].'-'.$this->request->data['User']['month'].'-'.$this->request->data['User']['day'];
				$this->User->create();
				$this->User->save($this->request->data);

				$this->Session->setFlash(__('You have successfully added this student.'), 'default', array('class' => 'alert alert-success'));
				$this->redirect("students_list");
			}
			

			
		}
		$this->request->data["User"]["date_enrolled"] = date("Y-m-d");
	}

	public function edit_student($id){
		if($this->request->is("post") || $this->request->is("put")){
			$this->request->data["User"]["birthdate"] = $this->request->data['User']['year'].'-'.$this->request->data['User']['month'].'-'.$this->request->data['User']['day'];
			$this->User->id = $id;
			$this->User->save($this->request->data);
			 $this->Session->setFlash(__('You have successfully updated this student.'), 'default', array('class' => 'alert alert-success'));
			$this->redirect("students_list");
		}
		else{
			$student = $this->User->findById($id);
			$student["User"]["user_name_display"] = $student["User"]["user_name"];
			$student["User"]["password_display"] = $student["User"]["temp_password"];
			$student["User"]["password"] = $student["User"]["temp_password"];
			$this->request->data = $student;
			$this->set("student", $student);
		}
	}

	public function teachers_list(){
		if ($this->request->is('post')) {
			$keyword = isset($this->data['User']['keyword']) ? $this->data['User']['keyword']:'';
			$conditions = "(User.first_name LIKE '%$keyword%' OR User.last_name  LIKE '%$keyword%' OR CONCAT(User.first_name,' ', User.last_name) LIKE '%$keyword%' OR User.receipt_no LIKE '%$keyword%' OR User.registration_number LIKE '%$keyword%' OR User.middle_initial LIKE '%$keyword%') AND User.type='teacher'";
			$teachers = $this->User->find('all', compact('conditions'));
		}
		else{
			$teachers = $this->User->findAllByType("teacher");
		}
		
		$this->set("teachers", $teachers);
	}

	public function ajax_check_user_name(){
		
		$first_name = $this->params["named"]["first_name"];
		$last_name = $this->params["named"]["last_name"];
		$user_exist = $this->User->findByFirstNameAndLastName($first_name,$last_name);
		$result = array(
			"error" => 0,
			"message"=> "",
			"name"=> "{$first_name} {$last_name}"
		);


		if($user_exist){

			$result = array(
				"error" => 1,
				"message"=> "This {$user_exist['User']['type']} already exists.",
				"name"=> "{$first_name} {$last_name}"
			);
		}
		
		$this->layout = "ajax";
		die(json_encode($result));

	}

	public function add_teacher(){
		if ($this->request->is('post')) {
			$this->request->data["User"]["type"]="teacher";
			$this->request->data["User"]["birthdate"] = $this->request->data['User']['year'].'-'.$this->request->data['User']['month'].'-'.$this->request->data['User']['day'];


			$this->request->data["User"]["first_name"] = ucwords($this->request->data["User"]["first_name"] );
			$this->request->data["User"]["last_name"] = ucwords($this->request->data["User"]["last_name"] );
			$teacher_exist = $this->User->findByFirstNameAndLastName($this->request->data["User"]["first_name"], $this->request->data["User"]["last_name"]);

			if($teacher_exist){
				$this->Session->setFlash(__('This teacher already exists.'), 'default', array('class' => 'alert alert-danger'));
			}
			else{
				$this->User->create();
				$this->User->save($this->request->data);

				$this->Session->setFlash(__('You have successfully added this teacher.'), 'default', array('class' => 'alert alert-success'));
				$this->redirect("teachers_list");
			}
			
		}
	}

	public function edit_teacher($id){
		if($this->request->is("post") || $this->request->is("put")){
			$this->request->data["User"]["birthdate"] = $this->request->data['User']['year'].'-'.$this->request->data['User']['month'].'-'.$this->request->data['User']['day'];
			$this->User->id = $id;
			$this->User->save($this->request->data);
			 $this->Session->setFlash(__('You have successfully updated this teacher.'), 'default', array('class' => 'alert alert-success'));
			$this->redirect("teachers_list");
		}
		else{
			$teacher = $this->User->findById($id);
			$teacher["User"]["user_name_display"] = $teacher["User"]["user_name"];
			$teacher["User"]["password_display"] = $teacher["User"]["temp_password"];
			$teacher["User"]["password"] = $teacher["User"]["temp_password"];
			$this->request->data = $teacher;
			$this->set("teacher", $teacher);
		}
	}

	public function account(){
		if($this->request->is("post") || $this->request->is("put")){
			if($this->User->exists($this->Auth->user("id"))){

				if($this->request->data["User"]["password"] != $this->request->data["User"]["password2"]){
					$this->Session->setFlash(__('Password does not match. Please try again.'), 'default', array('class' => 'alert alert-danger'));	
				}
				else{
					$this->User->id = $this->Auth->user("id");
					$this->User->save($this->request->data);
					$this->Session->setFlash(__('Your account was successfully updated.'), 'default', array('class' => 'alert alert-success'));
				}

				
			}
			else{
				$this->Session->setFlash(__('Something went wrong. Please try again.'), 'default', array('class' => 'alert alert-danger'));
			}
			$this->redirect("/users/account");
		}
		$user = $this->User->findById($this->Auth->user("id"));
		$this->request->data = $user;
	}

	public function export_students(){
		$this->User->recursive = 0;
		header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="openlearning_students.csv"'); 
        $fp = fopen('php://output', 'w');
 		fputcsv($fp, array());
        $headers = array(
        		'Id',
        		'Registration Number',
        		'Receipt Number',
        		'First Name',
        		'Middle Initial',
        		'Last Name',
        		'Birthday',
        		'Gender',
        		'Enrolled',
        		'Date Enrolled'
        		
        );

        $users = $this->User->findAllByType('student');
       
 		fputcsv($fp, $headers);
 		foreach ($users as $student) {
 			$data = array(
					$student['User']['id'],
					$student['User']['registration_number'],
					$student['User']['receipt_no'],
					$student['User']['last_name'],
					$student['User']['middle_initial'],
					$student['User']['first_name'],
					date_format(date_create($student['User']['birthdate']), "F, d Y h:i A"),
					$student['User']['gender'],
					$student['User']['is_enrolled'],
					date_format(date_create($student['User']['date_enrolled']), "F, d Y"),
			);

			fputcsv($fp, $data);
 		}
 		fclose($fp);
		exit();
	}

	public function export_teachers(){
		$this->User->recursive = 0;
		header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="openlearning_teachers.csv"'); 
        $fp = fopen('php://output', 'w');
 		fputcsv($fp, array());
        $headers = array(
        		'Id',
        		'First Name',
        		'Last Name',
        		'Birthday',
        		'Gender'
        		
        );

        $users = $this->User->findAllByType('teacher');
       
 		fputcsv($fp, $headers);
 		foreach ($users as $student) {
 			$data = array(
					$student['User']['id'],
					$student['User']['last_name'],
					$student['User']['first_name'],
					date_format(date_create($student['User']['birthdate']), "F, d Y h:i A"),
					$student['User']['gender']
			);

			fputcsv($fp, $data);
 		}
 		fclose($fp);
		exit();
	}
};

?>