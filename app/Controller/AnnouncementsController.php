<?php
/*all the functions for users adre here*/

App::uses('AppController', 'Controller');
class AnnouncementsController extends AppController {
	public function index(){	
		$this->Announcement->bindModel(array("belongsTo"=>array("User"))); 
		$order = "Announcement.id DESC";
		$announcements = $this->Announcement->find("all", compact("order"));
		$this->set("announcements", $announcements);
	}
	
	public function add(){
		if ($this->request->is('post')) {
			$this->Announcement->create();
			$this->request->data["Announcement"]["user_id"] = $this->Auth->user("id");
			$this->request->data["Announcement"]["date_added"] = date("Y-m-d H:i");
			$this->Announcement->save($this->request->data);

			$this->Session->setFlash(__('You have successfully added this announcement.'), 'default', array('class' => 'alert alert-success'));

			$this->redirect("index");
		}
	}

	public function edit($id){
		if($this->request->is("post") || $this->request->is("put")){
			$this->request->data["Announcement"]["user_id"] = $this->Auth->user("id");
			$this->Announcement->id=$id;
			$this->Announcement->save($this->request->data);

			$this->Session->setFlash(__('You have successfully updated this announcement.'), 'default', array('class' => 'alert alert-success'));

			$this->redirect("index");
		}
		else{
			$announcement = $this->Announcement->findById($id);
			$this->request->data = $announcement;
			$this->set("announcement", $announcement);
		}
	}

	public function delete($id){
		if($this->Announcement->exists($id)){
			$this->Announcement->id = $id;
			$this->Announcement->delete();
			$this->Session->setFlash(__('You have successfully delete this announcement.'), 'default', array('class' => 'alert alert-success'));
			$this->redirect("index");
		}
	}
};

?>