<?php
/*all the functions for users adre here*/

App::uses('AppController', 'Controller');
class SubjectsController extends AppController {
	public function index(){	
		$this->Subject->bindModel(array("belongsTo"=>array("User")));
		
		if($this->Auth->user("type") == "admin"){

			if ($this->request->is('post')) {
				$keyword = isset($this->data['Subject']['keyword']) ? $this->data['Subject']['keyword']:'';
				$conditions = "Subject.title LIKE '%$keyword%' OR Subject.description  LIKE '%$keyword%'";
				$subjects = $this->Subject->find('all', compact('conditions'));
			}
			else{
				$subjects = $this->Subject->find("all");
			}

			
		}
		else if($this->Auth->user("type") == "teacher"){
			if ($this->request->is('post')) {
				$keyword = isset($this->data['Subject']['keyword']) ? $this->data['Subject']['keyword']:'';
				$conditions = "(Subject.title LIKE '%$keyword%' OR Subject.description  LIKE '%$keyword%') AND Subject.user_id={$this->Auth->user("id")}";
				$subjects = $this->Subject->find('all', compact('conditions'));
			}
			else{
				$subjects = $this->Subject->find("all", array("conditions"=>array("user_id" => $this->Auth->user("id"))));
			}
		}
		else{
			//find all the subjects by students
			$this->redirect("students_subjects/".$this->Auth->user("id"));
		}
		
		
		$this->set("subjects", $subjects);
	}
	
	public function add(){
		if ($this->request->is('post')) {

			$exists = $this->Subject->findByTitleAndUserId($this->request->data["Subject"]["title"],$this->request->data["Subject"]["user_id"] );

			if(!$exists){
				$conflict_sched = $this->Subject->findByUserIdAndSchedule($this->request->data["Subject"]["user_id"],$this->request->data["Subject"]["schedule"] );


				if($conflict_sched){
					$this->Session->setFlash(__("Please enter another schedule. This subject has conflict with Subject: '{$conflict_sched["Subject"]["title"]}' scheduled at {$conflict_sched["Subject"]["schedule"]} also."), 'default', array('class' => 'alert alert-danger'));
				}
				else{
					$this->Subject->create();
					$this->Subject->save($this->request->data);

					$this->Session->setFlash(__('You have successfully added this subject.'), 'default', array('class' => 'alert alert-success'));

					$this->redirect("index");
				}
			}
			else{
				$this->Session->setFlash(__('You have already added this subject to this teacher.'), 'default', array('class' => 'alert alert-danger'));
			}

			
		}
		$this->loadModel("User");
		$this->User->virtualFields['full_name'] =  'CONCAT(User.first_name, " ", User.last_name)';
	
		$teachers = $this->User->find("list", array("fields"=>array("User.id", "full_name"), "conditions"=>array("User.type"=>"teacher")));

		$this->set("teachers", $teachers);
	}

	public function edit($id){
		if($this->request->is("post") || $this->request->is("put")){
			$this->Subject->id=$id;
			$this->Subject->save($this->request->data);

			$this->Session->setFlash(__('You have successfully updated this subject.'), 'default', array('class' => 'alert alert-danger'));

			$this->redirect("index");
		}
		else{
			$subject = $this->Subject->findById($id);
			$this->request->data = $subject;
			$this->set("subject", $subject);

			$this->loadModel("User");
			$this->User->virtualFields['full_name'] =  'CONCAT(User.first_name, " ", User.last_name)';
		
			$teachers = $this->User->find("list", array("fields"=>array("User.id", "full_name"), "conditions"=>array("User.type"=>"teacher")));

			$this->set("teachers", $teachers);
		}
	}

	public function view($id){
		if($this->Subject->exists($id)){
			$this->Subject->bindModel(array("belongsTo"=>array("User")));

			$subject = $this->Subject->findById($id);
			$this->set("subject", $subject);

			$this->loadModel("User");
			$conditions = "User.id in (SELECT user_id FROM 	students_subjects WHERE subject_id=$id)";
			$students = $this->User->find("all", compact("conditions"));

			$this->loadModel("StudentsSubject");

			foreach ($students as $key => $student) {
				$grade = $this->StudentsSubject->findByUserIdAndSubjectId($student["User"]["id"], $id);


				if($grade){
					$student["User"]["grade"] =  $grade["StudentsSubject"]["grade"];	
				}
				else{
					$student["User"]["grade"] =  "NA";
				}
				$students[$key] = $student;
			}
			$this->set("students", $students);
		}
		else{
			$this->Session->setFlash(__('Subject does not exists.'), 'default', array('class' => 'alert alert-danger'));
			$this->redirect("index");
		}

	}

	public function enroll_students($id){
		if($this->Subject->exists($id)){
			$this->Subject->bindModel(array("belongsTo"=>array("User")));
				
			$subject = $this->Subject->findById($id);
			$this->set("subject", $subject);

			$this->loadModel("User");
			$this->User->virtualFields['full_name'] =  'CONCAT(User.first_name, " ", User.last_name)';
			$conditions = "User.id AND User.type='student'";
			$fields = "User.id, User.full_name";
			$students = $this->User->find("list", compact("conditions", "fields"));
			$options=$students;
			$this->set("options", $students);

			$conditions = "User.id IN (SELECT user_id FROM 	students_subjects WHERE subject_id=$id) AND User.type='student'";
			$fields = "User.id";
			$selected = $this->User->find("list", compact("conditions", "fields"));
			$options=$students;
			$this->set("selected", $selected);
		}
		else{
			$this->Session->setFlash(__('Subject does not exists.'), 'default', array('class' => 'alert alert-danger'));
			$this->redirect("index");
		}

		if($this->request->is("post") || $this->request->is("put")){
			$this->loadModel("StudentsSubject");
			//$this->StudentsSubject->deleteAll(array("StudentsSubject.subject_id"=>$id));


			
			foreach ($this->request->data["Subject"]["student_ids"] as $key => $student_id) {


				$grade = $this->StudentsSubject->findByUserIdAndSubjectId($student_id, $id);

				if(!$grade){
					$this->StudentsSubject->create();
					$this->StudentsSubject->save(
						array(
							"user_id" => $student_id,
							"subject_id" => $id
						)
					);		
				}				
			}

			$student_ids = implode(",", $this->request->data["Subject"]["student_ids"]);

			$conditions="subject_id=$id AND user_id NOT IN($student_ids)";
			$fields = "StudentsSubject.id";
			$not_enrolled = $this->StudentsSubject->find("list", compact("conditions", "fields"));

			$not_enrolled_ids = implode(",", $not_enrolled);


			if($not_enrolled_ids){
				$conditions = "DELETE FROM students_subjects WHERE id IN ($not_enrolled_ids)";		
				$result = $this->StudentsSubject->query($conditions);

				debug($result);
			}

			$this->redirect("view/{$id}");
		}
	}

	public function add_grade($student_id, $subject_id){
		$this->set("student_id", $student_id);
		$this->set("subject_id", $subject_id);

		$this->loadModel("StudentsSubject");

		$grade_exist = $this->StudentsSubject->findByUserIdAndSubjectId($student_id, $subject_id);

		if($this->request->is("post") || $this->request->is("put")){
			

			if($grade_exist){
				$this->StudentsSubject->id = $grade_exist["StudentsSubject"]["id"];
			}
			else{
				$this->StudentsSubject->create();
			}

			
			$this->StudentsSubject->save(
				array(
					"user_id" => $student_id,
					"subject_id" => $subject_id,
					"grade" => $this->request->data["User"]["grade"]
				)
			);				
			$this->redirect("view/{$subject_id}");
		}
		else{
			if($grade_exist ){
				$this->request->data["User"]["grade"] = $grade_exist["StudentsSubject"]["grade"];
			}
			
		}

	}

	public function students_subjects($id){
		$this->loadModel("StudentsSubject");
		$this->loadModel("Subject");
		$students_subjects = $this->StudentsSubject->findAllByUserId($id);
		$subjects = [];

		foreach ($students_subjects as $key => $students_subject) {
			$this->Subject->bindModel(array("belongsTo"=>array("User")));
			$subject = $this->Subject->findById($students_subject["StudentsSubject"]["subject_id"]);
			if($subject){
				$subjects[$key] = $subject;
			$subjects[$key]["Subject"]["grade"] = $students_subject["StudentsSubject"]["grade"];
			}
			
		}
		$this->loadModel("User");
		$this->set("subjects", $subjects);
		$this->set("user", $this->User->findById($id));
	}

	public function quizzes($id, $student_id=null){
		$this->loadModel("Quiz");
		$this->Quiz->bindModel(array("belongsTo"=>array("User"), "hasOne"=>array("StudentsAnswer"))); 
		$quizzes = $this->Quiz->findAllBySubjectId($id);
		
		//debug($quizzes);
		$this->loadModel("QuizzesItem");

		//this code below computes the scores of the quiz
		$this->loadModel("StudentsAnswer");

		foreach ($quizzes as $key => $quiz) {
			//find score
			if($quiz["StudentsAnswer"]["id"]){
				$answers = json_decode($quiz["StudentsAnswer"]["answers"], true);

				$score = 0;

				if($answers){
					foreach ($answers as $item_id => $answer) {
						# code...
						$correct = $this->QuizzesItem->findByIdAndAnswer($item_id, strtolower($answer));

						if($correct){
							$score++;
						}
					}

					$quizzes[$key]["Score"] = "{$score}/{$quiz["Quiz"]["no_of_items"]}";	
				}


				//$this->StudentsAnswer->id = $quiz["StudentsAnswer"]["id"];
				//$this->StudentsAnswer->saveField("score",$score);

				
			}
			else{
					$quizzes[$key]["Score"] = "";
			}
		}

		if($student_id==null && $this->Auth->user("type")=="student"){
			$student_id = $this->Auth->user("id");
		}



		$this->set("quizzes", $quizzes);
		$this->set("student_id", $student_id);
		$this->set("subject", $this->Subject->findById($id));
	}

	public function upload_modules($id){
		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
		$subject = $this->Subject->findById($id);
		if ($this->request->is('post')) {

			$fileName = $this->request->data["Subject"]["doc_file"]["name"];
			$uploadPath = WWW_ROOT.DS."modules".DS."subjects".DS.$id.DS;

			$path = $uploadPath;
			$folder = new Folder($path, true, 0755);

			$uploadFile = $uploadPath.$fileName;
			if(move_uploaded_file($this->request->data["Subject"]["doc_file"]["tmp_name"],$uploadFile)){

				$this->loadModel("Module");

				$this->Module->create();
				$this->Module->save(array(
					"filename"=>$fileName,
					"path"=>$uploadFile,
					"user_id"=>$this->Auth->user("id"),
					"subject_id"=>$id,
					"date_added"=>date("Y-m-d H:i")
				));


				$this->Session->setFlash(__('You have successfully uploaded this module.'), 'default', array('class' => 'alert alert-success'));
				$this->redirect("/subjects/modules/{$id}");
			}
		}
	}


	public function modules($id){
		App::uses('Folder', 'Utility');
		App::uses('File', 'Utility');
		$subject = $this->Subject->findById($id);
		$this->set("subject", $subject);

		if ($this->request->is('post')) {

			$fileName = $this->request->data["Subject"]["doc_file"]["name"];
			$uploadPath = WWW_ROOT."modules".DS."subjects".DS.$id.DS;

			$path = $uploadPath;
			$folder = new Folder($path, true, 0755);

			$uploadFile = $uploadPath.$fileName;
			if(move_uploaded_file($this->request->data["Subject"]["doc_file"]["tmp_name"],$uploadFile)){

				$this->loadModel("Module");

				$this->Module->create();
				$this->Module->save(array(
					"filename"=>$fileName,
					"path"=>$uploadFile,
					"user_id"=>$this->Auth->user("id"),
					"subject_id"=>$id,
					"date_added"=>date("Y-m-d H:i")
				));


				$this->Session->setFlash(__('You have successfully uploaded this module.'), 'default', array('class' => 'alert alert-success'));
			}
		}
		else{
			$subject = $this->Subject->findById($id);
			$this->set("subject", $subject);
		}

		$this->loadModel("Module");
		$this->Module->bindModel(array("belongsTo"=>array("User")));
		$modules = $this->Module->findAllBySubjectId($id);
		$this->set("modules", $modules);
		
	}

	public function download($module_id) {
		$this->loadModel("Module");
		$module=$this->Module->findById($module_id);
	    $this->response->file($module["Module"]["path"], array(
	        'download' => true,
	        'name' =>  $module["Module"]["filename"]
	    ));
    	return $this->response;
	}

	public function teacher_subjects($teacher_id){
		$this->Subject->bindModel(array("belongsTo"=>array("User")));
		$subjects = $this->Subject->find("all", array("conditions"=>array("user_id" => $teacher_id)));
		$this->set("subjects", $subjects);

		$this->loadModel("User");
		$this->set("user", $this->User->findById($teacher_id));
	}

	public function export_teacher_subjects($teacher_id){
		$this->loadModel("User");
		header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="teacher_subjects.csv"'); 
        $fp = fopen('php://output', 'w');
 		fputcsv($fp, array());
        $headers = array(
        		'Id',
        		'Title',
        		'Description',
        		'Semester',
        		'Schedule',
        		'School Year'
        		
        );

        $this->Subject->bindModel(array("belongsTo"=>array("User")));
		$subjects = $this->Subject->find("all", array("conditions"=>array("user_id" => $teacher_id)));
       
 		fputcsv($fp, $headers);
 		foreach ($subjects as $subject) {
 			$data = array(
					$subject['Subject']['id'],
					$subject['Subject']['title'],
					$subject['Subject']['description'],
					$subject['Subject']['semester'],
					$subject['Subject']['schedule'],
					$subject['Subject']['school_year']
			);

			fputcsv($fp, $data);
 		}
 		fclose($fp);
		exit();
	}

	public function export_students_subjects($student_id){
		$this->loadModel("User");
		header('Content-Type: application/excel');
        header('Content-Disposition: attachment; filename="student_subjects.csv"'); 
        $fp = fopen('php://output', 'w');
 		fputcsv($fp, array());
        $headers = array(
        		'Id',
        		'Title',
        		'Description',
        		'Teacher',
        		'Semester',
        		'Schedule',
        		'School Year',
        		'Grade'
        		
        );

        $this->loadModel("StudentsSubject");
		$this->loadModel("Subject");
		$students_subjects = $this->StudentsSubject->findAllByUserId($student_id);
		$subjects = [];

		foreach ($students_subjects as $key => $students_subject) {
			$this->Subject->bindModel(array("belongsTo"=>array("User")));
			$subject = $this->Subject->findById($students_subject["StudentsSubject"]["subject_id"]);
			if($subject){
				$subjects[$key] = $subject;
			$subjects[$key]["Subject"]["grade"] = $students_subject["StudentsSubject"]["grade"];
			}
			
		}
       
 		fputcsv($fp, $headers);
 		foreach ($subjects as $subject) {
 			$data = array(
					$subject['Subject']['id'],
					$subject['Subject']['title'],
					$subject['Subject']['description'],
					"{$subject['User']['first_name']} {$subject['User']['last_name']}",
					$subject['Subject']['semester'],
					$subject['Subject']['schedule'],
					$subject['Subject']['school_year'],
					$subject['Subject']['grade']
			);

			fputcsv($fp, $data);
 		}
 		fclose($fp);
		exit();
	}

	 public function delete_module($id){
           $this->loadModel("Module");
            $this->Module->id = $id;
            $this->Module->delete();
            $this->Session->setFlash(__('You have successfully deleted this module.'), 'default', array('class' => 'alert alert-success')); 
            $this->redirect($this->referer());       
      }

};

?>		