<div class="container">
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Items to Quiz: <strong><?php echo $quiz["Quiz"]["title"];?></strong>
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create("Quiz", array("url"=>"/quizzes/edit_items/{$quiz["Quiz"]["id"]}"));?>
                    <div class="alert alert-success">
                        <strong><?php echo $quiz["Quiz"]["no_of_items"];?></strong> Questions
                    </div>
                        
                    <?php for ($i=0;$i<$quiz["Quiz"]["no_of_items"];$i++):?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
        
                                 <label><?php echo ( $i + 1);?>.)</label>
                                 <?php echo $this->Form->text("Quiz.{$i}.question", array("class"=>"form-control name", "required"=>true, "placeholder"=>"Enter question #".($i + 1), "style"=>"display:inline;width:89%;"));?>

                            </div>
                            <div class="panel-body">
                                 <?php echo $this->Form->text("Quiz.{$i}.id", array("class"=>"", "placeholder"=>"", "style"=>"display:none;"));?>

                                <div class="form-group col-md-6">
                                    <label>a.</label>
                                    <?php echo $this->Form->text("Quiz.{$i}.a", array("class"=>"form-control name", "required"=>true, "placeholder"=>"Enter choice a...", "style"=>"display:inline;width:80%;"));?>
                                </div>
                                 <div class="form-group col-md-6">
                                    <label>b.</label>
                                    <?php echo $this->Form->text("Quiz.{$i}.b", array("class"=>"form-control name", "required"=>true, "placeholder"=>"Enter choice b...", "style"=>"display:inline;width:80%;"));?>
                                </div>
                                 <div class="form-group col-md-6">
                                    <label>c.</label>
                                    <?php echo $this->Form->text("Quiz.{$i}.c", array("class"=>"form-control name", "required"=>true, "placeholder"=>"Enter choice c...", "style"=>"display:inline;width:80%;"));?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>d.</label>
                                    <?php echo $this->Form->text("Quiz.{$i}.d", array("class"=>"form-control name", "required"=>true, "placeholder"=>"Enter choice d...", "style"=>"display:inline;width:80%;"));?>
                                </div>
                                <div class="form-group col-md-6">
                                    <label>Answer:</label>
                                    <?php echo $this->Form->select("Quiz.{$i}.answer", 
                                        array("a"=>"a", "b"=>"b", "c"=>"c", "d"=>"d"),
                                        array("class"=>"form-control name", "required"=>true, "placeholder"=>"Enter answer e.g. a...", "style"=>"display:inline;width:20%;"));?>
                                </div>
                            </div>
                        </div>
                    <?php endfor;?>
                    <div class="col-md-12">
                        <button type="button submit" class="btn btn-success pull-right"><i class="glyphicon glyphicon-check"></i>Save</button>
                    </div>      
                    <?php echo $this->Form->end();?>
                </div>
                 
            </div>
        </div>
        
    </div>
    
</div>