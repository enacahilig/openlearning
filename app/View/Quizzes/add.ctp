<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Quiz
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create("Quiz", array("url"=>"/quizzes/add/{$subject_id}"));?>
                    <div class="alert alert-success">
                        <strong>Quiz Details</strong>
                    </div>
                        
                        <div class="form-group col-md-12">
                            <label>Title</label>
                            <?php echo $this->Form->text("title", array("class"=>"form-control name", "required"=>true, "placeholder"=>"Enter title..."));?>
                        </div>
                        <div class="form-group col-md-12">
                            <label>No. of Items</label>
                            <?php echo $this->Form->number("no_of_items", array("class"=>"form-control name", "placeholder"=>"Enter number of items...", "min"=>1));?>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Duration in Minutes</label>
                            <?php echo $this->Form->number("duration", array("class"=>"form-control name", "placeholder"=>"", "min"=>1));?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <button type="button submit" class="btn btn-success pull-right"><i class="glyphicon glyphicon-check"></i>Save</button>
                        </div>
                    
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
        
    </div>
    
</div>
