<div class="container">
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    Quiz: <strong><?php echo $quiz["Quiz"]["title"];?></strong>  
                    <span class="pull-right">
                        Answer:
                        <span class="answer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 
                    </span>
                </div>
                <div class="panel-body">

                    <?php foreach ($quiz["QuizzesItem"] as $index => $question):?>
                        <div>
                            <div class="col-md-12">
                                <label><?php echo ( $index + 1);?>.)</label>
                                <?php echo $question["question"];?>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <span class="<?php echo $question["answer"]=='a' ? 'answer' : '';?>">
                                        <label>a.</label>
                                        <?php echo $question["a"];?>
                                    </span>
                                </div>  
                                <div class="col-md-6 ">
                                    <span class="<?php echo $question["answer"]=='b' ? 'answer' : '';?>">
                                        <label>b.</label>
                                        <?php echo $question["b"];?>
                                    </span>
                                </div>  
                                <div class="col-md-6">
                                    <span class="<?php echo $question["answer"]=='c' ? 'answer' : '';?>">
                                        <label>c.</label>
                                        <?php echo $question["c"];?>
                                    </span>
                                </div>  
                                <div class="col-md-6">
                                    <span class="<?php echo $question["answer"]=='d' ? 'answer' : '';?>">
                                        <label>d.</label>
                                        <?php echo $question["d"];?>
                                    </span>
                                </div>  
                            </div>     
                        </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
        
    </div>
    
</div>