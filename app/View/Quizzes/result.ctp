<div class="container">
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    <div class="pull-left">
                        Student Name: <strong><?php echo "{$student["User"]["first_name"]} {$student["User"]["last_name"]}";?></strong> <br>
                        Subject: <strong><?php echo $subject["Subject"]["title"];?></strong><br>  
                    </div>
                    
                    <!-- <span class="pull-right">
                        &nbsp; Answer:
                        <span class="answer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 
                    </span>
                    &nbsp;&nbsp;
                    <span class="pull-right">
                        Your Answer:
                        <span class="your_answer">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> 
                    </span> -->
                    <div class="pull-right">
                       Quiz: <strong><?php echo $quiz["Quiz"]["title"];?></strong> <br>
                        Score:
                        <strong> <span class=""><?php    
                            $score = isset($quiz['Score']) ? $quiz['Score'] : 0;
                            echo $score; ?></span> 
                       </strong>
                    </div>
                    <br/><br/>
                </div>
                <div class="panel-body">

                    <?php foreach ($quiz["QuizzesItem"] as $index => $question):?>
                        <div>
                            <div class="col-md-12">
                                <?php if( $question["answer"]==$question["student_answer"]):?>
                                    <i class='glyphicon glyphicon-ok'></i>
                                <?php else:?>
                                     <i class='glyphicon glyphicon-remove'></i>
                                <?php endif;?>
                                
                               
                                <label><?php echo ( $index + 1);?>.)</label>
                                <?php echo $question["question"];?>
                            </div>
                            <div class="col-md-12">
                                <div class="col-md-6">
                                    <span class="<?php echo $question["answer"]=='a' ? 'answer' : '';?> <?php echo $question["answer"]!=$question["student_answer"] && $question["student_answer"]=='a' ? 'your_answer' : '';?>">
                                        <label>a.</label>
                                        <?php echo $question["a"];?>
                                    </span>
                                </div>  
                                <div class="col-md-6 ">
                                    <span class="<?php echo $question["answer"]=='b' ? 'answer' : '';?> <?php echo $question["answer"]!=$question["student_answer"] && $question["student_answer"]=='b' ? 'your_answer' : '';?>">
                                        <label>b.</label>
                                        <?php echo $question["b"];?>
                                    </span>
                                </div>  
                                <div class="col-md-6">
                                    <span class="<?php echo $question["answer"]=='c' ? 'answer' : '';?> <?php echo $question["answer"]!=$question["student_answer"] && $question["student_answer"]=='c' ? 'your_answer' : '';?>">
                                        <label>c.</label>
                                        <?php echo $question["c"];?>
                                    </span>
                                </div>  
                                <div class="col-md-6">
                                    <span class="<?php echo $question["answer"]=='d' ? 'answer' : '';?> <?php echo $question["answer"]!=$question["student_answer"] && $question["student_answer"]=='d' ? 'your_answer' : '';?>">
                                        <label>d.</label>
                                        <?php echo $question["d"];?>
                                    </span>
                                </div>  
                            </div>     
                        </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
        
    </div>
    
</div>