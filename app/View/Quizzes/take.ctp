<div class="container">
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    Quiz: <strong><?php echo $quiz["Quiz"]["title"];?></strong>  
                    <div class="pull-right">
                       Duration: <strong><?php echo $quiz["Quiz"]["duration"];?> minutes</strong> 
                       Time Left: <strong><span id="clock"></span></strong>
                    </div>
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create("Quiz", array("action"=>"/take/{$quiz["Quiz"]["id"]}", "id"=>"QuizForm"));?>
                    <?php foreach ($quiz["QuizzesItem"] as $index => $question):?>
                        <div>
                            <div class="col-md-1">
                                <?php echo $this->Form->text("Quiz.{$index}.answer", array("class"=>"form-control name answers", "required"=>true, "placeholder"=>"", "data-question-id"=>$question["id"]));?>
                                <?php echo $this->Form->text("Quiz.{$index}.question_id", array("class"=>"form-control name", "required"=>true, "placeholder"=>"", "style"=>"display:none;", "value"=>$question["id"]));?>
                            </div>
                            <div class="col-md-11">
                                <label><?php echo ( $index + 1);?>.)</label>
                                <?php echo $question["question"];?>
                                 <div class="col-md-12">
                                <div class="col-md-6">
                                    <span class="">
                                        <label>a.</label>
                                        <?php echo $question["a"];?>
                                    </span>
                                </div>  
                                <div class="col-md-6 ">
                                    <span class="">
                                        <label>b.</label>
                                        <?php echo $question["b"];?>
                                    </span>
                                </div>  
                                <div class="col-md-6">
                                    <span class="">
                                        <label>c.</label>
                                        <?php echo $question["c"];?>
                                    </span>
                                </div>  
                                <div class="col-md-6">
                                    <span class="">
                                        <label>d.</label>
                                        <?php echo $question["d"];?>
                                    </span>
                                </div>  
                            </div> 
                            </div>
                               
                        </div>
                    <?php endforeach;?>
                    <div class="col-md-12">
                        <button type="button submit" class="btn btn-success pull-right"><i class="glyphicon glyphicon-check"></i>Submit</button>
                    </div> 
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
        
    </div>
    
</div>
<?php echo $this->Html->script("jquery.countdown");?>
<script type="text/javascript">

    $(document).ready( function(){  

        $('#clock').countdown("<?php echo $end_date;?>", function(event) {
            var totalHours = event.offset.totalDays * 24 + event.offset.hours;
            $(this).html(event.strftime(totalHours + ' hr %M min %S sec'));

        })
        .on('finish.countdown', function(event) {
           alert("Times up! Submitting your quiz right now...");
           setTimeout( function(){
              $("#QuizForm").submit();
           },1000);
           
        });

        $(".answers").keyup( function(){
            var question_id = $(this).attr("data-question-id");
            var answer = $(this).val();
            var quiz_id = "<?php echo $quiz["Quiz"]["id"];?>";

            $.ajax({
                url: "<?php echo $this->base;?>/quizzes/ajax_save_user_answer/question_id:"+ question_id + "/quiz_id:" + quiz_id + "/answer:" + answer,
                success: function(result) {
                    console.log(result);
                    result = JSON.parse(result);
                    if(result.error == 1){
                        $(".error2").html(result.message);
                        $(".error2").removeClass("hide");                
                    }
                    else{
                        $(".error2").html("");
                        $(".error2").addClass("hide");  
                    }
                   
                }
            });

        });
   
    });
</script>
