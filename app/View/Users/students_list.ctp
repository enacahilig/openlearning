<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">students <?php echo $this->Html->link("Add <i class='glyphicon glyphicon-plus'></i>", "/users/add_student", array("class"=>"btn btn-default btn-success", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;"));?> 

                <?php echo $this->Html->link("Export <i class=' glyphicon glyphicon-export'></i>", "/users/export_students", array("class"=>"btn btn-default btn-success", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;"));?>
            </h1>
            
    
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-lg-6">
            <?php echo $this->Form->create('User', array("action"=>"students_list", "id"=>"search")); ?>
            <div class="input-group">
                 <?php echo $this->Form->text("keyword", array("class"=>"form-control", "placeholder"=>"Enter keyword..."));?>
                
                <span class="input-group-btn">
                    <?php echo $this->Form->submit('Search', array('class'=>'btn btn-default')); ?>
            
                </span>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
     
    </div>
   
    <br>
    <?php if($students):?>
       <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <br/>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Registration Number</th>
                                    <th>Receipt Number</th>
                                    <th>Name</th>
                                    <th>Username</th>
                                    <th>Password</th>
                                    <th>Birthday</th>
                                    <th>Gender</th>
                                    <th>Enrolled</th>
                                    <th>Date Enrolled</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($students as $key => $student): ?>
                                <tr>
                                    <td><?php echo $student['User']['id'];?></td>
                                    <td><?php echo $student['User']['registration_number'];?></td>
                                    <td><?php echo $student['User']['receipt_no'];?></td>
                                    <td>
                                        <?php echo $student['User']['first_name'];?>&nbsp;
                                        <?php echo $student['User']['middle_initial'];?>&nbsp;
                                        <?php echo $student['User']['last_name'];?>
                                     </td>
                                    <td><?php echo $student['User']['user_name'];?></td>
                                    <td><?php echo $student['User']['temp_password'];?></td>
                                    <td><?php echo $student['User']['birthdate'];?></td>
                                    <td><?php echo $student['User']['gender'];?></td>
                                    <td><?php echo $student['User']['is_enrolled'];?></td>
                                     <td><?php echo $student['User']['date_enrolled'];?></td>
                                    <td>
                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-list'></i> View Subjects", "/subjects/students_subjects/{$student["User"]["id"]}", array("class"=>"btn btn-info editBtn", "escape"=>false));?>
                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "/users/edit_student/{$student["User"]["id"]}", array("class"=>"btn btn-default btn-success editBtn", "escape"=>false));?>
                                        <?php //echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/users/delete/{$student['User']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?"));?>
                                    </td>
                                </tr>

                                <?php endforeach;?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                   
        </div>
    </div>
    <?php else:?>
        No student added yet.
    <?php endif;?>  
</div>
   
     

