<div class="clearfix"></div>
<div class="container">
    <div class="row">
            

            <div class="col-md-4 col-md-offset-4">
            <br>
                    
                  
                    <div class="login-form" >
                     <h4>User Login</h4>
                    <?php echo $this->Form->create("User");?>
                        <div class="form-group">
                            <?php echo $this->Form->text('username', array("class"=>"form-control login-field", "placeholder"=>"Enter your username"));?>
                          
                            <label class="login-field-icon fui-user" for="login-name"></label>
                        </div>

                        <div class="form-group">
                            <?php echo $this->Form->password('password', array("class"=>"form-control login-field", "placeholder"=>"Password"));?>
                          
                            <label class="login-field-icon fui-lock" for="login-pass"></label>
                        </div>

                        <div class="form-group">
                            <?php echo $this->Form->select('type', array("student"=>"Student", "teacher"=>"Teacher", "admin"=>"Admin") ,array("class"=>"form-control login-field", "empty"=>false));?>
                          
                            <label class="login-field-icon" for="login-pass"></label>
                        </div>

                        <button type="submit" class="btn btn-lg btn-primary btn-block">Login</button>
                   
                    
                    </div>
                   <?php echo $this->Form->end();?>
            </div> 
            
        
    </div>
</div>