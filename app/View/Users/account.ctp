<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    Update Account
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create("User", array("url"=>"/users/account"));?>
                    <div class="alert alert-success">
                        <strong>Account Details</strong>
                    </div>
                        
                        <div class="form-group">
                            <label>Username</label>
                            <?php echo $this->Form->text("user_name", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter username..."));?>
                        </div>
                        <div class="form-group">
                            <label>New Password</label>
                            <?php echo $this->Form->password("password", array("class"=>"form-control", "required"=>true, "placeholder"=>"Enter new password...", "value"=>""));?>
                        </div>
                        <div class="form-group">
                            <label>Retype New Password</label>
                            <?php echo $this->Form->password("password2", array("class"=>"form-control", "required"=>true, "placeholder"=>"Retype password...", "value"=>""));?>
                        </div>
                         <div class="col-md-12">
                            <button type="button submit" class="btn btn-success pull-right"><i class="glyphicon glyphicon-check"></i>Save</button>
                        </div>
                    
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
        
    </div>
    
</div>
