<div class="container">
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Student
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create("User", array("url"=>"/users/add_student"));?>
                    <div class="alert alert-success">
                        <strong>Enrollment details</strong>
                    </div>
                    <div class="alert alert-danger error hide">
                    </div>
                        <div class="form-group col-md-6 col-lg-6">
                            <label>Registration No. </label>
                            <?php echo $this->Form->text("registration_number", array("class"=>"form-control", "required"=>true));?>

                        </div>
                        <div class="form-group col-md-6 col-lg-6">
                            <label>Receipt No.</label>
                            <?php echo $this->Form->text("receipt_no", array("class"=>"form-control", "required"=>true));?>
                        </div>
                        <div class="form-group col-md-6 col-lg-6">
                            <label>Enrolled</label>
                            <?php echo $this->Form->select("is_enrolled",array("Enrolled"=>"Enrolled", "Pending"=>"Pending"), array("class"=>"form-control", "required"=>true,"empty"=>false));?>
                        </div>
                        <div class="form-group col-md-6 col-lg-6">
                            <label>Date Enrolled</label>
                            <br>
                            <?php echo $this->Form->date("date_enrolled",array("class"=>"form-control ", "required"=>true));?>
                        </div>
                        <div class="clearfix">
                        </div>
                        <div class="alert alert-success">
                            <strong>Personal Details</strong>
                        </div>  
                        <div class="alert alert-danger error2 hide">
                        </div>   
                        <div class="form-group col-md-4">
                            <label>First Name</label>
                            <?php echo $this->Form->text("first_name", array("class"=>"form-control name NameInputs", "required"=>true, "placeholder"=>"Enter first name..."));?>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Middle Initial</label>
                            <?php echo $this->Form->text("middle_initial", array("class"=>"form-control name NameInputs", "required"=>true, "placeholder"=>"Enter middle initial..."));?>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Last Name</label>
                            <?php echo $this->Form->text("last_name", array("class"=>"form-control name NameInputs", "required"=>true, "placeholder"=>"Enter last name..."));?>
                        </div>
                        <div class="form-group col-md-6 col-lg-6">
                            <label>User Name</label>
                            <?php echo $this->Form->text("user_name_display", array("class"=>"form-control", "required"=>true, "disabled"=>true));?>
                            <?php echo $this->Form->text("user_name", array("class"=>"form-control hide", "required"=>true));?>
                        </div>
                         <div class="form-group col-md-6 col-lg-6">
                            <label>Password</label>
                            <?php echo $this->Form->text("password_display", array("class"=>"form-control", "required"=>true,  "disabled"=>true));?>
                            <?php echo $this->Form->password("password", array("class"=>"form-control hide", "required"=>true));?>
                            <?php echo $this->Form->password("temp_password", array("class"=>"form-control hide", "required"=>true));?>
                        </div>
                        <div class="form-group col-md-6 col-lg-6">
                            <label>Birthdate</label>
                            <br>
                            <?php echo $this->Form->select("day",null,array("class"=>"form-control ",  "id"=>"day","required"=>true, "empty"=>false));?>
                            <?php echo $this->Form->select("month",null,array("class"=>"form-control",  "id"=>"month","required"=>true, "empty"=>false));?>
                           
                            <?php echo $this->Form->select("year",null,array("class"=>"form-control",  "id"=>"year","required"=>true, "empty"=>false));?>
                           
                        </div>
                        <div class="form-group col-md-6 col-lg-6">
                            <label>Gender</label>
                            <?php echo $this->Form->select("gender",array("M"=>"Male", "F"=>"Female"),  array("class"=>"form-control",  "required"=>true,"empty"=>false));?>
                        </div>
                        
                        <div class="clearfix"></div>
                       
                        <div class="col-md-12">
                            <button type="button submit" class="btn btn-success pull-right"><i class="glyphicon glyphicon-check"></i>Save</button>
                        </div>
                    
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
        
    </div>
    
</div>
<?php echo $this->Html->script("dobPicker.min");?>
<script type="text/javascript">
    $(document).ready( function(){
        $.dobPicker({
            daySelector: '#day', /* Required */
            monthSelector: '#month', /* Required */
            yearSelector: '#year', /* Required */
            minimumAge: 8, /* Optional */
            maximumAge: 100 /* Optional */
        });

        $.dobPicker({
            daySelector: '#day1', /* Required */
            monthSelector: '#month1', /* Required */
            yearSelector: '#year1', /* Required */
            minimumAge: 8, /* Optional */
            maximumAge: 100 /* Optional */
        });

        $(".name").change( function(){
            var user_name = $("#UserFirstName").val() + "." + $("#UserLastName").val() + "_" + Math.random().toString(36).substr(2, 5);
            var password = Math.random().toString(36).substr(2, 6);
            $("#UserUserNameDisplay").val(user_name);
            $("#UserPasswordDisplay").val(password);
            $("#UserTempPassword").val(password);
            $("#UserPassword").val(password);
            $("#UserUserName").val(user_name);
        });

        $("#UserRegistrationNumber").keyup( function(){
            keyword =  $(this).val();
            $.ajax({
                url: "<?php echo $this->base;?>/users/ajax_check_registration_number/keyword:"+ keyword,
                success: function(result) {
                    result = JSON.parse(result);
                    console.log(result);
                    if(result.error == 1){
                        $(".error").html(result.message);
                        $(".error").removeClass("hide");                
                    }
                    else{
                        $(".error").html("");
                        $(".error").addClass("hide");  
                    }
                   
                }
            });

        });

        $(".NameInputs").keyup( function(){
            var first_name =  $("#UserFirstName").val();
            var last_name =  $("#UserLastName").val();
            $.ajax({
                url: "<?php echo $this->base;?>/users/ajax_check_user_name/first_name:"+ first_name + "/last_name:" + last_name,
                success: function(result) {
                    console.log(result);
                    result = JSON.parse(result);
                    if(result.error == 1){
                        $(".error2").html(result.message);
                        $(".error2").removeClass("hide");                
                    }
                    else{
                        $(".error2").html("");
                        $(".error2").addClass("hide");  
                    }
                   
                }
            });

        });
    });   
</script>
