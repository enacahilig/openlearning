<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Announcement
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create("Announcement", array("url"=>"/announcements/add"));?>
                    <div class="alert alert-success">
                        <strong>Announcement Details</strong>
                    </div>
                        
                        <div class="form-group col-md-12">
                            <label>Title</label>
                            <?php echo $this->Form->text("title", array("class"=>"form-control name", "required"=>true, "placeholder"=>"Enter title..."));?>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Content</label>
                            <?php echo $this->Form->textarea("content", array("class"=>"form-control name", "placeholder"=>"Enter content...", "rows"=>10));?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <button type="button submit" class="btn btn-success pull-right"><i class="glyphicon glyphicon-check"></i>Save</button>
                        </div>
                    
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
        
    </div>
    
</div>
<?php echo $this->Html->script('tinymce/tinymce.min'); ?>
<script>tinymce.init({
  selector: "textarea",
  height: 300,
  plugins: [
    "advlist autolink autosave link lists charmap print preview hr anchor pagebreak spellchecker",
    "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
    "table contextmenu directionality emoticons template textcolor paste fullpage textcolor colorpicker textpattern"
  ],

  menubar: false,
  toolbar_items_size: 'small',

  style_formats: [{
    title: 'Bold text',
    inline: 'b'
  }, {
    title: 'Red text',
    inline: 'span',
    styles: {
      color: '#ff0000'
    }
  }, {
    title: 'Red header',
    block: 'h1',
    styles: {
      color: '#ff0000'
    }
  }, {
    title: 'Example 1',
    inline: 'span',
    classes: 'example1'
  }, {
    title: 'Example 2',
    inline: 'span',
    classes: 'example2'
  }, {
    title: 'Table styles'
  }, {
    title: 'Table row 1',
    selector: 'tr',
    classes: 'tablerow1'
  }],

  templates: [{
    title: 'Test template 1',
    content: 'Test 1'
  }, {
    title: 'Test template 2',
    content: 'Test 2'
  }]
});</script>