<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">announcements 
            <?php if($logged_in_user["User"]["type"]!="student"):?>
                <?php echo $this->Html->link("Add <i class='glyphicon glyphicon-plus'></i>", "/announcements/add", array("class"=>"btn btn-default btn-success", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;"));?>
            <?php endif;?>
            </h1>
            
            
    
        </div>
    </div>
    <div class="clearfix"></div>
    <br>
    <?php if($announcements):?>
       <div class="row">
        <div class="col-md-12">
           
                <?php foreach ($announcements as $key => $announcement): ?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <?php echo $announcement['Announcement']['title'];?>
                            <?php if($logged_in_user["User"]["type"]!="student"):?>        
                                <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>&nbsp;", "/announcements/delete/{$announcement["Announcement"]["id"]}", array("class"=>"btn btn-default btn-danger editBtn pull-right", "escape"=>false));?>                
                                <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "/announcements/edit/{$announcement["Announcement"]["id"]}", array("class"=>"btn btn-default btn-success editBtn pull-right", "escape"=>false));?>

                            <?php endif;?>
                        </div>  
                        <div class="panel-body">
                            By: <?php echo $announcement['User']['first_name'];?> <?php echo $announcement['User']['last_name'];?>
                            <br>
                            Date Posted:  <?php echo date_format(date_create($announcement['Announcement']['date_added']), "F, d Y h:i A");?>
                            <br>
                            <?php echo $announcement['Announcement']['content'];?>
                        </div>
                    </div>  
                <?php endforeach;?>      
        </div>
    </div>
    <?php else:?>
        No announcement added yet.
    <?php endif;?>  
</div>
   
     

