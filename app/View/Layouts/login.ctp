<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Open Learning</title>

    <!-- Bootstrap Core CSS -->
    <?php echo $this->Html->css("bootstrap");?>
    <?php echo $this->Html->css("flat-ui.min");?>
    <!-- Custom Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css" type="text/css">

    <!-- Plugin CSS -->
   

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<style type="text/css">
body, .btn-primary{
    background: #425065;
}
h4{
    
    text-align: center;
    color: #425065;
}

</style>

<body>
   
    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-lg-4 col-lg-offset-4"> 
            <?php echo $this->Session->flash(); ?>
        </div>
    </div>
        <?php echo $this->fetch('content'); ?>

    <!-- jQuery -->
    <?php echo $this->Html->script("jquery");?>
    <?php echo $this->Html->script("bootstrap.min");?>
    <?php echo $this->Html->script("flat-ui.min");?>
   
 

</body>

</html>
