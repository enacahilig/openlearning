<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */


?>
<!DOCTYPE html>
<html>
<head>
    <?php echo $this->Html->meta(
        'favicon.ico',
        '/favicon.ico',
        array('type' => 'icon')
    );
    ?>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Dashboard">
    <meta name="keyword" content="Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">

    <title>Online Open Learning</title>
    <?php echo $this->Html->meta('icon'); ?>
    <?php echo $this->Html->css('bootstrap.min'); ?>
    
    <?php echo $this->Html->css('style');?>
    <?php echo $this->Html->css('custom');?>
   
    <?php echo $this->Html->script("jquery");?>

    
    
    
        
    
</head>
<style type="text/css">

</style>

<body>
<?php if( $logged_in_user ):?>
<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo $this->base;?>/users/dashboard" style="padding-top:12px;color:white;">Online Open Learning</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
             <?php if($logged_in_user["User"]["type"] != "student") :?>

                    <li class="<?php echo $this->params['controller']=='announcements' &&  ($this->params['action']=='add' || $this->params['action']=='edit' || $this->params['action']=='index') ? 'menu-top-active' : '';?>">
                        <?php echo $this->Html->link("Announcements", "/announcements", array("escape"=>false));?>
                    </li>
                
                    
               
                <?php if($logged_in_user["User"]["type"] == "admin"):?>
                    <li class="<?php echo $this->params['controller']=='users' &&  ($this->params['action']=='students_list' || $this->params['action']=='add_student' || $this->params['action']=='edit_student'  || $this->params['action']=='view_grades') ? 'menu-top-active' : '';?>">
                        <?php echo $this->Html->link("Students", "/users/students_list", array("escape"=>false));?>
                    </li>
                    
                    <li class="<?php echo $this->params['controller']=='users' &&  ($this->params['action']=='teachers_list' || $this->params['action']=='add_teacher' || $this->params['action']=='edit_teacher') ? 'menu-top-active' : '';?>">
                        <?php echo $this->Html->link("Teachers", "/users/teachers_list", array("escape"=>false));?>
                    </li>
                 <?php endif;?>

                
            <?php else :?>
                <li class="<?php echo $this->params['controller']=='announcements' &&  ($this->params['action']=='add' || $this->params['action']=='edit' || $this->params['action']=='index') ? 'menu-top-active' : '';?>">
                    <?php echo $this->Html->link("Announcements", "/announcements", array("escape"=>false));?>
                </li>
            <?php endif;?>

            <li class="<?php echo $this->params['controller']=='subjects' &&  ($this->params['action']=='index' || $this->params['action']=='add' || $this->params['action']=='edit' || $this->params['action']=='view') ? 'menu-top-active' : '';?>">
                    <?php echo $this->Html->link("Subjects", "/subjects", array("escape"=>false));?>
            </li>

            <li class="<?php echo $this->params['controller']=='users' &&  ($this->params['action']=='account') ? 'menu-top-active' : '';?>">
                 <?php echo $this->Html->link("{$logged_in_user["User"]["user_name"]}", "/users/account", array("escape"=>false, "style" =>"color:white"));?>
            </li>
            <li>
                <?php echo $this->Html->link("Log Out", "/users/logout", array("escape"=>false, "style" =>"color:white"));?>
            </li>
             <?php echo $this->Html->link("Back", $back_link , array("class"=>"btn btn-default btn-info editBtn", "escape"=>false, "style"=>"margin-top:7px"));?>
        </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
    <div class="content-wrapper" style="">
       <?php echo $this->Session->flash(); ?>
        <?php echo $this->fetch('content'); ?>
    </div>
    <!-- CONTENT-WRAPPER SECTION END-->
    <footer style="margin-top:160px;">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    &copy; 2015 YourCompany | By : <a href="http://www.designbootstrap.com/" target="_blank">DesignBootstrap</a>
                </div>

            </div>
        </div>
    </footer>
<?php endif;?>
    <?php echo $this->Html->script("bootstrap.min");?>
    
    

</body>
</html>
<script type="text/javascript">
    $(document).ready( function(){
        //$(".btn-danger").hide();
    })
</script>
