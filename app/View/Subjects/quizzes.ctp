<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line"><?php echo $subject["Subject"]["title"];?>'s Quizzes 
            <?php if($logged_in_user["User"]["type"]!="student"):?>
                <?php echo $this->Html->link("Add <i class='glyphicon glyphicon-plus'></i>", "/quizzes/add/{$subject["Subject"]["id"]}", array("class"=>"btn btn-default btn-success", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;"));?>
            <?php endif;?>
            </h1>
            
            
    
        </div>
    </div>
    <div class="clearfix"></div>
    <br>
    <?php if($quizzes):?>
       <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <br/>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Title</th>
                                    <th>No. of Items</th>
                                    <th>Duration</th>
                                    <th>Date Added</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($quizzes as $key => $quiz): ?>
                                <tr>
                                    <td><?php echo $quiz['Quiz']['id'];?></td>
                                    <td><?php echo $quiz['Quiz']['title'];?></td>
                                    <td><?php echo $quiz['Quiz']['no_of_items'];?></td>
                                    <td><?php echo $quiz['Quiz']['duration'];?></td>
                                    <td><?php echo $quiz['Quiz']['date_added'];?></td>
                                    <td>
                                        <?php if($logged_in_user["User"]["type"]!="student"){?> 
                                            <?php  if(!$quiz['StudentsAnswer']['done']):?>
                                                  <?php echo $this->Html->link("<i class='glyphicon glyphicon-list'></i> View Quiz Details", "/quizzes/view/{$quiz['Quiz']["id"]}", array("class"=>"btn btn-info btn-success editBtn", "escape"=>false));?>  

                                            <?php else:?>
                                                <?php if($student_id):?>
                                                    <?php echo $this->Html->link("View Student's Results", "/quizzes/result/{$quiz['Quiz']["id"]}/{$student_id}", array("class"=>"btn btn-info btn-success editBtn", "escape"=>false));?>  
                                                <?php else:?>
                                                    <?php echo $this->Html->link("<i class='glyphicon glyphicon-list'></i> View  Quiz Details", "/quizzes/view/{$quiz['Quiz']["id"]}", array("class"=>"btn btn-info btn-success editBtn", "escape"=>false));?>  
                                                <?php endif;?>
                                            <?php endif;?>
                                               
                                             <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "/quizzes/edit/{$quiz['Quiz']["id"]}", array("class"=>"btn btn-default btn-success editBtn", "escape"=>false));?>

                                            <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/quizzes/delete/{$quiz['Quiz']["id"]}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "onclick"=>"return confirm('Are you sure you want to delete this quiz?');"));?>
                                        <?php } else if ($logged_in_user["User"]["type"]=="student"){ ?>
                                            <?php 
                                                if(!$quiz['StudentsAnswer']['done']){
                                                    echo $this->Html->link("Take Quiz", "/quizzes/take/{$quiz['Quiz']["id"]}", array("class"=>"btn btn-default btn-info editBtn", "escape"=>false));  
                                                }
                                                else{
                                                    //score here
                                                    echo $this->Html->link("<i class='glyphicon glyphicon-list'></i> View My Results", "/quizzes/result/{$quiz['Quiz']["id"]}/{$student_id}", array("class"=>"btn btn-info btn-success editBtn", "escape"=>false));
                                                    echo "&nbsp;&nbsp;&nbsp;";
                                                    $score = isset($quiz['Score']) ? $quiz['Score'] : 0;
                                                    echo "Score:".$score;
                                                     
                                                }
                                                ?>
                                        <?php  }?>
                                    </td>
                                </tr>

                                <?php endforeach;?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                   
        </div>
    </div>
    <?php else:?>
        No quiz added yet.
    <?php endif;?>  
</div>
   
     

