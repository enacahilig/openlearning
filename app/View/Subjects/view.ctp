<div class="container-fluid">
    <div class="row">
        <div class="col-md-12"
>            <h1 class="page-head-line">
                <?php echo $subject["Subject"]["title"];?> 
                <?php if($logged_in_user["User"]["type"]=="admin"):?>
                    <?php echo $this->Html->link("Enroll Students <i class='glyphicon glyphicon-plus'></i>", "/subjects/enroll_students/{$subject["Subject"]["id"]}", array("class"=>"btn btn-default btn-success", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;"));?>
                <?php endif;?>
                <br>
                <span style="font-size: 12px;color:#6f6a6a"><?php echo $subject['User']['first_name'];?> <?php echo $subject['User']['last_name'];?></span>
            </h1>
        </div>
    </div>
    <div class="clearfix"></div>
   
    <br>
    <?php if($students):?>
       <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <br/>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>First Name</th>
                                    <th>Last Name</th>
                                    <th>Grade</th>
                                    <?php if($logged_in_user["User"]["type"] != "admin"):?>
                                        <th>Actions</th>
                                    <?php endif;?>
                                   
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($students as $key => $student): ?>
                                <tr>
                                    <td><?php echo $student['User']['id'];?></td>
                                    <td><?php echo $student['User']['first_name'];?></td>
                                    <td><?php echo $student['User']['last_name'];?></td>
                                    <td><?php echo $student['User']['grade'];?></td>
                                    <?php if($logged_in_user["User"]["type"] != "admin"):?>
                                        <td>
                                            <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i> Update Grade", "/subjects/add_grade/{$student["User"]["id"]}/{$subject["Subject"]["id"]}", array("class"=>"btn btn-default btn-success editBtn", "escape"=>false));?>

                                            <?php echo $this->Html->link("View Student's Quizzes", "/subjects/quizzes/{$subject["Subject"]["id"]}/{$student["User"]["id"]}", array("class"=>"btn btn-info btn-success editBtn", "escape"=>false));?>  
                                            <?php //echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/users/delete/{$student['User']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?"));?>
                                        </td>
                                    <?php endif;?>
                                </tr>

                                <?php endforeach;?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                   
        </div>
    </div>
    <?php else:?>
        No student enrolled in this subject yet.
    <?php endif;?>  
</div>
   
     

