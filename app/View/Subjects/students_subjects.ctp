<div class="container-fluid">
    <div class="row">
        <div class="col-md-12"
>            <h1 class="page-head-line">
                <?php echo $user["User"]["first_name"] ;?>'s Subjects
                 <?php echo $this->Html->link("Export <i class='glyphicon glyphicon-export'></i>", "/subjects/export_students_subjects/{$user["User"]["id"]}", array("class"=>"btn btn-default btn-success", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;"));?> 
            </h1>
        </div>
    </div>
    <div class="clearfix"></div>
   
    <br>
    <?php if($subjects):?>
       <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <br/>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Teacher</th>
                                    <th>Semester</th>
                                    <th>School Year</th>
                                    <th>Grade</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($subjects as $key => $subject): ?>
                                <tr>
                                    <td><?php echo $subject['Subject']['id'];?></td>
                                    <td><?php echo $subject['Subject']['title'];?></td>
                                    <td><?php echo $subject['Subject']['description'];?></td>
                                    <td><?php echo $subject['User']['first_name'];?> <?php echo $subject['User']['last_name'];?></td>
                                    <td><?php echo $subject['Subject']['semester'];?></td>
                                    <td><?php echo $subject['Subject']['school_year'];?></td>
                                    <td><?php echo $subject['Subject']['grade'] ? $subject['Subject']['grade'] : 'NA';?>
                                         
                                    </td>
                                    <td>
                                         <?php echo $this->Html->link("<i class='glyphicon glyphicon-check'></i> Quizzes", "/subjects/quizzes/{$subject["Subject"]["id"]}/{$user["User"]["id"]}", array("class"=>"btn btn-default btn-info editBtn", "escape"=>false));?>
                                        <?php echo $this->Html->link("<i class='glyphicon glyphicon-book'></i> Modules", "/subjects/modules/{$subject["Subject"]["id"]}", array("class"=>"btn btn-default btn-info editBtn", "escape"=>false));?>
                                    </td>
                                </tr>

                                <?php endforeach;?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                   
        </div>
    </div>
    <?php else:?>
        No subjects added yet.
    <?php endif;?>  
</div>
   
     

