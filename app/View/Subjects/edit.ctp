<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    Edit Subject
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create("Subject", array("url"=>"/subjects/edit/{$subject["Subject"]["id"]}"));?>
                    <div class="alert alert-success">
                        <strong>Subject Details</strong>
                    </div>
                        
                        <div class="form-group col-md-12">
                            <label>Title</label>
                            <?php echo $this->Form->text("title", array("class"=>"form-control name", "required"=>true, "placeholder"=>"Enter subject..."));?>
                        </div>
                        <div class="form-group col-md-12">
                            <label>Description</label>
                            <?php echo $this->Form->text("description", array("class"=>"form-control name", "required"=>true, "placeholder"=>"Enter description..."));?>
                        </div>
                        <div class="form-group col-md-12 col-lg-12">
                            <label>Teacher</label>
                            <?php echo $this->Form->select("user_id", $teachers, array("class"=>"form-control", "required"=>true, "empty"=>false));?>
                        </div>
                        <div class="form-group col-md-12 col-lg-12">
                            <label>Semeter</label>
                            <?php echo $this->Form->text("semester", array("class"=>"form-control", "required"=>true, "placeholder"=>"e.g. 1st"));?>
                        </div>
                        <div class="form-group col-md-12 col-lg-12">
                            <label>Schedule</label>
                            <?php echo $this->Form->text("schedule", array("class"=>"form-control", "required"=>true, "placeholder"=>"e.g. 8:30AM-9:30AM"));?>
                        </div>
                         <div class="form-group col-md-12 col-lg-12">
                            <label>School Year</label>
                            <?php echo $this->Form->text("school_year", array("class"=>"form-control", "required"=>true, "placeholder"=>"e.g. 2017-2018"));?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <button type="button submit" class="btn btn-success pull-right"><i class="glyphicon glyphicon-check"></i>Save</button>
                        </div>
                    
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
        
    </div>
    
</div>
