<div class="container">
    <div class="row">
        <div class="col-md-4 col-md-offset-4" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    Add Grade
                </div>
                <div class="panel-body">
                    <?php echo $this->Form->create("User", array("url"=>"/subjects/add_grade/{$student_id}/{$subject_id}"));?>
                    <div class="alert alert-success">
                        <strong>Grade Details</strong>
                    </div>
                        
                        <div class="form-group col-md-12">
                            <label>Grade</label>
                            <?php echo $this->Form->text("grade", array("class"=>"form-control name", "required"=>true, "placeholder"=>"Enter student's grade"));?>
                        </div>
                        <div class="col-md-12">
                            <button type="button submit" class="btn btn-success pull-right"><i class="glyphicon glyphicon-check"></i>Save</button>
                        </div>
                    
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
        
    </div>
    
</div>



