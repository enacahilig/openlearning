
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line"><?php echo $subject["Subject"]["title"];?>'s Module 
            <?php if($logged_in_user["User"]["type"]!="student"):?>
                <?php echo $this->Html->link("Upload <i class='glyphicon glyphicon-upload'></i>", "#", array("class"=>"btn btn-default btn-success", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;", "data-toggle"=>"modal","data-target"=>"#uploadPhoto"));?>
            <?php endif;?>
            </h1>
            
            
    
        </div>
    </div>
    <div class="clearfix"></div>
    <br>
   <?php if($modules):?>
       <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <br/>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Filename</th>
                                    <th>Date Added</th>
                                    <th>Actions</th>
                            
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($modules as $key => $module): ?>
                                <tr>
                                    <td><?php echo $module["Module"]['id'];?></td>
                                    <td><?php echo $module["Module"]['filename'];?></td>
                                    <td>
                                         <?php echo date_format(date_create($module['Module']['date_added']), "F, d Y h:i A");?>
                                    </td>
                                    
                                    <td>
                                        
                                       <?php echo $this->Html->link("<i class='glyphicon glyphicon-download'></i>", "/subjects/download/{$module['Module']['id']}", array("class"=>"btn btn-default btn-info", "escape"=>false));?>

                                         <?php if($logged_in_user["User"]["type"] != "student"):?>
                                            <?php echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/subjects/delete_module/{$module['Module']['id']}/{$subject["Subject"]["id"]}", array("onclick"=>"return confirm('Are you sure you want to delete this module?');", "class"=>"btn btn-default btn-danger", "escape"=>false));?>
                                        <?php endif;?>
                                    </td>
                                </tr>

                                <?php endforeach;?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                   
        </div>
    </div>
    <?php else:?>
        No modules added yet.
    <?php endif;?> 
</div>

<div class="modal fade" id="uploadPhoto" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title" id="myModalLabel">Upload Module</h4>
          </div>
          <div class="modal-body">
                <?php echo $this->Form->create("Subject", array("url"=>"/subjects/upload_modules/{$subject["Subject"]['id']}","type"=>"file"));?>

          
                <div class="form-group">

                    <?php echo $this->Form->input('doc_file',array( 'type' => 'file',  "class"=>"file"));?>
                </div>
                <?php echo $this->Form->end();?>
          </div>
        </div>
      </div>
</div>

<link rel="stylesheet" type="text/css" href="<?php echo $this->base;?>/css/fileinput.css">
<script src="<?php echo $this->base;?>/js/fileinput.js"></script>