<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <h1 class="page-head-line">subjects
                <?php if($logged_in_user["User"]["type"]=="admin"):?>
                    <?php 
                        echo $this->Html->link("Add <i class='glyphicon glyphicon-plus'></i>", "/subjects/add", array("class"=>"btn btn-default btn-success", "escape"=>false, "style"=>"padding:3px 5px 3px 5px;font-size:12px;"));?></h1>
                <?php endif;?>     
            
    
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
        <div class="col-lg-6">
            <?php echo $this->Form->create('Subject', array("action"=>"index", "id"=>"search")); ?>
            <div class="input-group">
                 <?php echo $this->Form->text("keyword", array("class"=>"form-control", "placeholder"=>"Enter keyword..."));?>
                
                <span class="input-group-btn">
                    <?php echo $this->Form->submit('Search', array('class'=>'btn btn-default')); ?>
            
                </span>
            </div>
            <?php echo $this->Form->end(); ?>
        </div>
     
    </div>
    <br>
    <?php if($subjects):?>
       <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <br/>
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Teacher</th>
                                    <th>Semester</th>
                                    <th>Schedule</th>
                                    <th>School Year</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($subjects as $key => $subject): ?>
                                <tr>
                                    <td><?php echo $subject['Subject']['id'];?></td>
                                    <td><?php echo $subject['Subject']['title'];?></td>
                                    <td><?php echo $subject['Subject']['description'];?></td>
                                    <td><?php echo $subject['User']['first_name'];?> <?php echo $subject['User']['last_name'];?></td>
                                    <td><?php echo $subject['Subject']['semester'];?></td>
                                    <td><?php echo $subject['Subject']['schedule'];?></td>
                                    <td><?php echo $subject['Subject']['school_year'];?></td>

                                    <td>
                                       
                                        <?php if($logged_in_user["User"]["type"]=="admin"):?>
                                            <?php echo $this->Html->link("<i class='glyphicon glyphicon-pencil'></i>", "/subjects/edit/{$subject["Subject"]["id"]}", array("class"=>"btn btn-default btn-success editBtn", "escape"=>false));?>                          
                                        <?php endif;?>
                                         <?php echo $this->Html->link("<i class='glyphicon glyphicon-user'></i> View Students", "/subjects/view/{$subject["Subject"]["id"]}", array("class"=>"btn btn-default btn-info editBtn", "escape"=>false));?>
                                         <?php echo $this->Html->link("<i class='glyphicon glyphicon-check'></i> Quizzes", "/subjects/quizzes/{$subject["Subject"]["id"]}", array("class"=>"btn btn-default btn-info editBtn", "escape"=>false));?>
                                         <?php echo $this->Html->link("<i class='glyphicon glyphicon-book'></i> Modules", "/subjects/modules/{$subject["Subject"]["id"]}", array("class"=>"btn btn-default btn-info editBtn", "escape"=>false));?>
                                        <?php //echo $this->Html->link("<i class='glyphicon glyphicon-trash'></i>", "/users/delete/{$student['User']['id']}", array("class"=>"btn btn-default btn-danger", "escape"=>false, "confirm"=>"Are you sure?"));?>
                                    </td>
                                </tr>

                                <?php endforeach;?>
                                
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
                   
        </div>
    </div>
    <?php else:?>
        No subjects added yet.
    <?php endif;?>  
</div>
   
     

