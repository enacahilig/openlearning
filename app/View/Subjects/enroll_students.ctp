<div class="container">
    <div class="row">
        <div class="col-md-12" >
            <div class="panel panel-default">
                <div class="panel-heading">
                    <?php echo strtoupper($subject["Subject"]["title"]);?>
                </div>

                <div class="panel-body">
                    <div class="">
                        Choose the students you want to enroll:
                    </div>
                    <?php echo $this->Form->create("Subject", array("url"=>"/subjects/enroll_students/{$subject["Subject"]["id"]}"));?>     

                        <div>
                            <?php
                                echo $this->Form->input('student_ids', array('multiple' => 'checkbox', 'options' => $options, 'selected' => $selected, "label"=>false));
                            ;?>      
                        </div>
                        <div class="clearfix"></div>
                        <div class="col-md-12">
                            <button type="button submit" class="btn btn-success pull-right"><i class="glyphicon glyphicon-check"></i>Enroll</button>
                        </div>
                    
                    <?php echo $this->Form->end();?>
                </div>
            </div>
        </div>
        
    </div>
    
</div>
<script type="text/javascript">
    $(".student_ids_checkbox").click(function(){
       // alert($(this).val());
       // alert($(this).attr("data-id"));
        alert($(this).is(":checked"));
    });
</script>
